#!/bin/sh

#  checkUUIDs.sh
#  MailTags4
#
#  Created by Scott Morrison on 2016-07-05.
#
#Script to check PluginCompatibilityUUID UUIDS on Mail and Message and update project Info.plist if there are new values

#exit 0

osxBuild=`sw_vers | grep BuildVersion | cut -f2 -s`
osxVersion=`sw_vers -productVersion`
osxMajorVersion=`sw_vers -productVersion | cut -f1 -d"." -s`
osxMinorVersion=`sw_vers -productVersion | cut -f2 -d"." -s`
echo "OSx Version ${osxVersion}"
SCSVersion=`defaults read "${PROJECT_DIR}/${INFOPLIST_FILE}" "CFBundleVersion" | cut -f1 -d "." -s`

mailPath="/Applications/Mail.app"
if [ $osxMajorVersion -ge 12 ] ; then
    echo "Monterey"
    infoPlistKeyName="Supported${osxMajorVersion}.${osxMinorVersion}PluginCompatibilityUUIDs"
    mailPath="/System/Applications/Mail.app"
elif [ $osxMajorVersion -eq 11 ] ; then
    echo "We are on Big Sur"
    infoPlistKeyName="Supported${osxMajorVersion}.${osxMinorVersion}PluginCompatibilityUUIDs"
    mailPath="/System/Applications/Mail.app"
    if [ $SCSVersion -le 2019 ]; then
        echo 'building pre bigSur version so do not update 10.16 uuids'
        exit 0
    fi
elif [ $osxMinorVersion -eq 15 ] ; then
    echo "We are on Catalina"
    infoPlistKeyName="Supported10.15PluginCompatibilityUUIDs"
    mailPath="/System/Applications/Mail.app"
    if [ $SCSVersion -lt 2 ]; then
        echo 'building pre catalina version so do not update 10.15 uuids'
        exit 0
    fi
elif [ $osxMinorVersion -ge 14 ] ; then
    infoPlistKeyName="Supported10.14PluginCompatibilityUUIDs"
elif [ $osxMinorVersion -ge 13 ] ; then
    infoPlistKeyName="Supported10.13PluginCompatibilityUUIDs"
elif [ $osxMinorVersion -ge 12 ] ; then
    infoPlistKeyName="Supported10.12PluginCompatibilityUUIDs"
else
    infoPlistKeyName="SupportedPluginCompatibilityUUIDs"
fi
echo "Mail path: ${mailPath}"

mailVersion=`defaults read "${mailPath}/Contents/Info.plist" CFBundleShortVersionString`
mailMajorVersion=`defaults read "${mailPath}/Contents/Info.plist" CFBundleShortVersionString | cut -f1 -d "." -s`

echo "mailMajorVersion: ${mailMajorVersion}"

echo "infoPlistKeyName: ${infoPlistKeyName}"

mailuuid=`defaults read ${mailPath}/Contents/Info.plist PluginCompatibilityUUID`

# Check Mail's UUID
if [ ! `defaults read ${PROJECT_DIR}/${INFOPLIST_FILE} "${infoPlistKeyName}" | grep ${mailuuid}` ]; then
    echo "Adding new uuid for MAIL: ${mailuuid}"

    mailBuild=`defaults read "${mailPath}/Contents/Info.plist" CFBundleVersion`

    defaults write ${PROJECT_DIR}/${INFOPLIST_FILE} "${infoPlistKeyName}" -array-add "#For Mail ${mailVersion}b${mailBuild} on ${osxVersion} build ${osxBuild}"
    defaults write ${PROJECT_DIR}/${INFOPLIST_FILE} "${infoPlistKeyName}" -array-add ${mailuuid}
    plutil -convert xml1 ${PROJECT_DIR}/${INFOPLIST_FILE}
fi
#uuid=`uuidgen`
#
#defaults write ${PROJECT_DIR}/${INFOPLIST_FILE} "SCBuildUUID" "${uuid}"
#plutil -convert xml1 ${PROJECT_DIR}/${INFOPLIST_FILE}

