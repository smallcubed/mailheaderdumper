#!/bin/sh

#  CopyHeaders.sh
#  MailDump
#
#  Created by Scott Morrison on 2020-10-16.
#  Copyright © 2020 smallcubed. All rights reserved.

exit 0
osascript -e "beep"

logger -t MailDump "COPYING Headers"
MAIL_HEADERS_SRC="${HOME}/Library/Mail/Headers/"
MAIL_HEADERS_DEST="${SRCROOT}/../Mail_Headers/"

logger  "cp -r \"${MAIL_HEADERS_SRC}\" \"${MAIL_HEADERS_DEST}\""
cp -r "${MAIL_HEADERS_SRC}"* "${MAIL_HEADERS_DEST}"
osascript -e "beep"
