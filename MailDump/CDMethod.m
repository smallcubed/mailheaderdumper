//
//  CDMethod.m
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-09.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import "CDMethod.h"
#import "CDParameter.h"
#import "CDType.h"
#import "NSScanner+methodLineScanner.h"
#import "CDContext.h"

@interface CDMethod ()
@property (weak) CDContext * context;
@end

@implementation CDMethod
+(instancetype)withObjcClassMethod:(Method)m context:(CDContext*)context{
    SEL _methodName = method_getName(m);
    if (_methodName){
        
        CDMethod * mthd = [CDMethod new];
        mthd.selector = [@"+" stringByAppendingString:NSStringFromSelector(_methodName)];
        [mthd setTypesFromSignature:method_getTypeEncoding(m) context:context];
        if (mthd.declaration == nil){
            
        }
        
        return mthd;
    }
    return nil;
}

+(instancetype)withObjcInstanceMethod:(Method)m context:(CDContext*)context{
    SEL _methodName = method_getName(m);
    if (_methodName){
        
        CDMethod * mthd = [CDMethod new];
        mthd.selector = [@"-" stringByAppendingString:NSStringFromSelector(_methodName)];
        if ([mthd.selector isEqualToString:@"-_menuUsingInfo:length:"]){
            
        }
        [mthd setTypesFromSignature:method_getTypeEncoding(m) context:context];
        return mthd;
    }
    return nil;
}


+(instancetype)withObjcMethodDescription:(struct objc_method_description) md context:(CDContext*) context isInstanceMethod:(BOOL)isInstanceMethod{
    CDMethod * mthd = [CDMethod new];
    mthd.selector = [NSString stringWithFormat:@"%c%@",isInstanceMethod?'-':'+' ,NSStringFromSelector(md.name)];
    [mthd setTypesFromSignature:md.types context:context];

    return mthd;
}

-(void)setTypesFromSignature:(const char*)methodsignature context:(CDContext*) context{
    NSScanner * sc = [NSScanner scannerWithString: [NSString stringWithUTF8String:methodsignature]];
    sc.caseSensitive = YES;
    NSInteger idx = 0;
    NSMutableArray * params = [NSMutableArray new];

    while (!sc.isAtEnd){
        NSString * typeEncoding;
        if ([sc scanObjcTypeEncoding:&typeEncoding]){
            if (idx==0){
                CDType * type = [CDType withObjcEncoding:typeEncoding context:context];
                if (type){
                    self.returnType = type;
                }
            }
            else if (idx >2){
                CDType * type = [CDType withObjcEncoding:typeEncoding context:context];
                if (type){
                    CDParameter * param  = [CDParameter new];
                    param.type = type;
                    param.name = [NSString stringWithFormat:@"arg%ld",idx-2];
                    [params addObject:param];
                }
            }
            [sc scanInteger:nil];
            idx++;
        }
        else{
            break;
        }
    }
    self.parameters = params;
}



-(instancetype)initWithLine:(NSString*)line{
    self = [self init];
    if (self){
        NSCharacterSet * whiteSpaceCS = [NSCharacterSet whitespaceCharacterSet];
        
        NSScanner * scanner = [NSScanner scannerWithString:line];
        [scanner setCharactersToBeSkipped:nil];
        NSString * methodType = nil;
        NSMutableString * muSelector = [NSMutableString string];
        if ([scanner scanUpToCharactersFromString:@"(" intoString:&methodType]){
            [muSelector appendString:[methodType stringByTrimmingCharactersInSet:whiteSpaceCS]];
            [scanner advance:1];
        }
        CDType * returnType ;
        if ([scanner scanUpToCharacters:@")" intoCDType:&returnType name:nil]){
            [scanner advance:1];
            self.returnType = returnType;
        }
        NSMutableArray <CDParameter*> * parameters= [NSMutableArray new];
        NSCharacterSet * openParentCS = [NSCharacterSet characterSetWithCharactersInString:@"("];
        [scanner scanCharactersFromSet:whiteSpaceCS intoString:nil];
        while (![scanner isAtEnd]){
            NSString * selectorChunk = nil;
            if ([scanner scanUpToCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:@":;"] intoString:&selectorChunk]){
                [muSelector appendString:selectorChunk];
                if ([scanner scanCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:@":"] intoString:nil]){
                    [muSelector appendString:@":"];
                    [scanner scanUpToCharactersFromSet:openParentCS intoString:nil];
                    CDParameter *param = [CDParameter new];

                    if ([scanner currentCharacter]=='('){
                        [scanner advance:1];
                        CDType  * paramType;
                        if([scanner scanUpToCharacters:@")" intoCDType:&paramType name:nil]){
                            [scanner advance:1];
                            param.type = paramType;
                        }
                    }
                    
                    [scanner scanWhiteSpace];
                    NSString * paramName;
                    [scanner scanUpToCharactersFromString:@" ;\t" intoString:&paramName];
                    [scanner advance:1];
                    param.name = paramName;
                    [scanner scanWhiteSpace];
                    [parameters addObject:param];
                }
            }
            if ([scanner scanCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:@";"] intoString:nil]){
                break;
            }
        }
        self.selector = [muSelector copy];
        if ([muSelector isEqualToString:@"+frameForTornOffWindow"]){
            
        }
        self.parameters = parameters;
    }
    return self;
}
+(CDMethod*)methodWithLine:(NSString*)line{
    CDMethod * mtd = [[CDMethod alloc] initWithLine:line];
    return mtd;
}
-(NSString*)headerDeclaration{
    NSMutableString * decl = [[NSMutableString alloc] init];
    [decl appendFormat:@"%@ (%@)",[self.selector substringToIndex:1],self.returnType.typeName];
   
    NSArray * selectorParts = [[self.selector substringFromIndex:1] componentsSeparatedByString:@":"];
    [selectorParts enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [decl appendString:obj];
        if (idx < self.parameters.count){
            [decl appendFormat: @":(%@)arg%ld ",self.parameters[idx].type,idx+1];
        }
    }];
    return [[decl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] stringByAppendingString:@";"];
}
-(NSString*)declaration{
    NSMutableString * decl = [[NSMutableString alloc] init];
    [decl appendFormat:@"%@ (%@)",[self.selector substringToIndex:1],self.returnType.declaration];
    
    NSArray * selectorParts = [[self.selector substringFromIndex:1] componentsSeparatedByString:@":"];
    [selectorParts enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [decl appendString:obj];
        if (idx < self.parameters.count){
            [decl appendFormat: @":%@ ",self.parameters[idx].methodParameterDeclaration];
        }
    }];
     return [[decl stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] stringByAppendingString:@";"];
}
-(NSArray  <CDType*> *) referencedStructs{
    NSMutableSet <NSString*> * structNames = [NSMutableSet new];
    NSMutableArray <CDType*> * structs = [NSMutableArray new];
    
    for (CDType * structMember in self.returnType.referencedStructs){
        if (![structNames containsObject:structMember.typeName]){
            [structs addObject:structMember];
            [structNames addObject:structMember.typeName];
        }
    }
    for (CDParameter * param in self.parameters){
        for (CDType * structMember in param.type.referencedStructs){
            if (![structNames containsObject:structMember.typeName]){
                [structs addObject:structMember];
                [structNames addObject:structMember.typeName];
            }
        }
    }
    return structs;
}

-(NSArray <NSString*> * ) referencedClassNames{
    NSMutableSet * muClasses = [NSMutableSet new];
    [muClasses addObjectsFromArray:[self.returnType referencedClassNames]];
    for (CDParameter * param in self.parameters){
        [muClasses addObjectsFromArray:param.referencedClassNames];
    }
    
    return [muClasses allObjects];
}
-(NSArray <NSString*> * ) referencedProtocolNames{
    NSMutableSet * muProtocols = [NSMutableSet new];
    [muProtocols addObjectsFromArray:[self.returnType referencedProtocolNames]];
    for (CDParameter * param in self.parameters){
        [muProtocols addObjectsFromArray:param.referencedProtocolNames];
    }
     return [muProtocols allObjects];
}
-(CDMethod*)copyWithZone:(NSZone*)zone{
    CDMethod * obj = [CDMethod new];
    obj.selector = self.selector;
    obj.returnType = [self.returnType copy];
    obj.parameters = [self.parameters copy];
    return obj;
}
-(NSUInteger)hash{
    return [self.selector hash];
}
-(BOOL)isEqual:(CDMethod*)obj{
    if ([obj isKindOfClass:[CDMethod class]]){
        return [obj.selector isEqualToString:self.selector];
    }
    return NO;
}
@end
