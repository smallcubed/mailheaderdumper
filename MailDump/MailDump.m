//
//  MailDump.m
//  MailDump
//
//  Created by Scott Morrison on 2019-10-31.
//  Copyright © 2019 smallcubed. All rights reserved.
//

#import "MailDump.h"
#import "Cocoa/Cocoa.h"

#import <objc/objc-runtime.h>

#import "CDContext.h"
#import "CDType.h"
#import "CDProperty.h"
#import "CDMethod.h"
#import "CDProtocol.h"
#import "CDClass.h"

#import "NSScanner+methodLineScanner.h"

#define FAKE_LOCALSTRING(string) MDnonLocalizedString(string)

__unused __attribute__((annotate("returns_localized_nsstring"))) static inline NSString *MDnonLocalizedString(NSString *s){return s;}

#import <mach-o/dyld.h>
#import <mach-o/loader.h>

static NSUUID *ExecutableUUID(void)
{
    const struct mach_header *executableHeader = NULL;
    for (uint32_t i = 0; i < _dyld_image_count(); i++)
    {
        const struct mach_header *header = _dyld_get_image_header(i);
        if (header->filetype == MH_EXECUTE)
        {
            executableHeader = header;
            break;
        }
    }

    if (!executableHeader)
        return nil;

    BOOL is64bit = executableHeader->magic == MH_MAGIC_64 || executableHeader->magic == MH_CIGAM_64;
    uintptr_t cursor = (uintptr_t)executableHeader + (is64bit ? sizeof(struct mach_header_64) : sizeof(struct mach_header));
    const struct segment_command *segmentCommand = NULL;
    for (uint32_t i = 0; i < executableHeader->ncmds; i++, cursor += segmentCommand->cmdsize)
    {
        segmentCommand = (struct segment_command *)cursor;
        if (segmentCommand->cmd == LC_UUID)
        {
            const struct uuid_command *uuidCommand = (const struct uuid_command *)segmentCommand;
            return [[NSUUID alloc] initWithUUIDBytes:uuidCommand->uuid];
        }
    }

    return nil;
}


@interface NSString (MailDump)
- (NSString*)stringWithPathRelativeTo:(NSString*)anchorPath;
@end

@implementation NSString (MailDump)
- (NSString*)stringWithPathRelativeTo:(NSString*)anchorPath {
    NSArray *pathComponents = [self pathComponents];
    NSArray *anchorComponents = [anchorPath pathComponents];

    NSInteger componentsInCommon = MIN([pathComponents count], [anchorComponents count]);
    for (NSInteger i = 0, n = componentsInCommon; i < n; i++) {
        if (![[pathComponents objectAtIndex:i] isEqualToString:[anchorComponents objectAtIndex:i]]) {
            componentsInCommon = i;
            break;
        }
    }

    NSUInteger numberOfParentComponents = [anchorComponents count] - componentsInCommon;
    NSUInteger numberOfPathComponents = [pathComponents count] - componentsInCommon;

    NSMutableArray *relativeComponents = [NSMutableArray arrayWithCapacity:
                                          numberOfParentComponents + numberOfPathComponents];
    for (NSInteger i = 0; i < numberOfParentComponents; i++) {
        [relativeComponents addObject:@".."];
    }
    [relativeComponents addObjectsFromArray:
     [pathComponents subarrayWithRange:NSMakeRange(componentsInCommon, numberOfPathComponents)]];
    return [NSString pathWithComponents:relativeComponents];
}
@end

@implementation MailDump
+(void)load{
    dispatch_async(dispatch_get_main_queue(),^{
        [self delayedLoad];
    });
    
}

+(void)delayedLoad{
    
    NSDictionary *versionDict = [NSDictionary dictionaryWithContentsOfFile:@"/System/Library/CoreServices/SystemVersion.plist"];
    NSString * osVersion = [NSString stringWithFormat:@"%@ (%@)",versionDict[@"ProductVersion"], versionDict[@"ProductBuildVersion"]];
    NSString * currentOsBuild = versionDict[@"ProductBuildVersion"];
    NSString * mailUUID = [ExecutableUUID() UUIDString];
    NSDictionary * launchArgs = [[NSUserDefaults standardUserDefaults] volatileDomainForName:NSArgumentDomain];
    
    NSFileManager * fm = [NSFileManager defaultManager];
  
    NSString * lastBuild = [[NSUserDefaults standardUserDefaults] stringForKey:@"com.smallcubed.maildump.lastBuild"];

    BOOL dumpAway = ![currentOsBuild isEqualToString:lastBuild]|| [launchArgs[@"dumpHeaders"] boolValue] || [launchArgs[@"dump-dsym"] boolValue];
    if (dumpAway){
        NSBundle * bundle = [NSBundle bundleForClass:self];
        NSString * bundleVersion = bundle.infoDictionary[(NSString*)kCFBundleVersionKey];
        
        BOOL showSavePanel = NO;
      
        NSData * bookmark =  [[NSUserDefaults standardUserDefaults] dataForKey:@"com.smallcubed.maildump.bookmark"];
        BOOL stale = NO;
        
        NSError * error =nil;
        NSURL * pathURL = [NSURL URLByResolvingBookmarkData:bookmark options:NSURLBookmarkResolutionWithSecurityScope relativeToURL:nil bookmarkDataIsStale:&stale error:&error];
      
        __block NSString * path = nil;
        if (!pathURL){
            showSavePanel = YES;
            path = [NSString stringWithUTF8String:__FILE__];
            while (path.length && ![[path lastPathComponent] isEqualToString:@"SCS"]){
                path = [path stringByDeletingLastPathComponent];
            }
            path = [path stringByAppendingPathComponent:@"Mail_Headers"];
            pathURL = [NSURL fileURLWithPath:path];
            
            NSURL * testFileURL = [pathURL URLByAppendingPathComponent:@"test.txt"];
        
            if (stale || ![[NSData new] writeToURL:testFileURL options:0 error:&error]){
                showSavePanel = YES;
            }
            else{
                [fm removeItemAtURL:testFileURL error:&error];
            }
            path = pathURL.path;
           
             if (showSavePanel){
                 __block BOOL done = NO;
              
                NSOpenPanel * savePanel = [NSOpenPanel  openPanel];
                savePanel.canChooseFiles = NO;
                savePanel.canChooseDirectories= YES;
                savePanel.canCreateDirectories = YES;
                savePanel.directoryURL =[NSURL fileURLWithPath:path];;
                [savePanel beginWithCompletionHandler:^(NSModalResponse result) {
                    if (result == NSModalResponseOK){
                         path =  [savePanel.URL path];
                        NSError * error = nil;
                        NSData * bookmark = [savePanel.URL bookmarkDataWithOptions:NSURLBookmarkCreationWithSecurityScope
                                                    includingResourceValuesForKeys:nil
                                                                     relativeToURL:nil
                                                                             error:&error];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:bookmark forKey:@"com.smallcubed.maildump.bookmark"];
                    }
                    done = YES;
                }];
                while (!done){
                    NSEvent * event = [NSApp nextEventMatchingMask:NSEventMaskAny untilDate:[NSDate dateWithTimeIntervalSinceNow:.01] inMode:NSDefaultRunLoopMode dequeue:YES];
                    [NSApp sendEvent:event];
                }
            }
        }
        else{
            [pathURL startAccessingSecurityScopedResource];
            path = [pathURL path];
        }
        NSLog (@"Loaded MailDump.mailbundle %@",bundleVersion);
        NSLog (@"Dumping headers to path %@",pathURL.absoluteString);
        
        NSString * dsymFolderPath = [path stringByAppendingPathComponent:@"DSYMs"];
         if (![fm fileExistsAtPath:dsymFolderPath]){
            [fm createDirectoryAtPath:dsymFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        NSString * dsymMapFolderPath = [path stringByAppendingPathComponent:@"DSYM_map"];
        if (![fm fileExistsAtPath:dsymMapFolderPath]){
            [fm createDirectoryAtPath:dsymMapFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        NSString * dsymFileName = [mailUUID stringByAppendingPathExtension:@"dSYM"];
        
        NSString * dsymFilePath = [dsymFolderPath stringByAppendingPathComponent:dsymFileName] ;
        if ([fm fileExistsAtPath:dsymFilePath]){
            [fm removeItemAtPath:dsymFilePath error:nil];
        }
        NSString * classDumpPath = [ [NSBundle bundleForClass:self] pathForAuxiliaryExecutable:@"class-dump"];
        
        NSURL * classDumpURL = [NSURL fileURLWithPath:classDumpPath];
        NSArray * args = @[@"-o",dsymFolderPath,@"--dsym",@"--dsym-name-uuid", NSBundle.mainBundle.executablePath];
        NSError * taskError = nil;
        NSTask * task = [NSTask launchedTaskWithExecutableURL:classDumpURL arguments:args error:&taskError terminationHandler:^(NSTask * _Nonnull _task) {
        }];
        [task waitUntilExit];
        if ([fm fileExistsAtPath:dsymFilePath]){
            NSString * buildNumDsymFileName = [currentOsBuild stringByAppendingPathExtension:@"dSYM"];
            NSString * buildNumDsymFilePath = [dsymFolderPath stringByAppendingPathComponent:buildNumDsymFileName];
            
            NSError * fmError;
            if ([fm fileExistsAtPath:buildNumDsymFilePath]){
                fmError = nil;
                [fm removeItemAtPath:buildNumDsymFilePath error:&fmError];
            }
            fmError = nil;
            [fm moveItemAtPath:dsymFilePath toPath:buildNumDsymFilePath error:&fmError];
            
            NSString * fileMapUUID = [[mailUUID stringByReplacingCharactersInRange:NSMakeRange(4, 0) withString:@"-"] stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
            NSString * fileMapUUIDParent = [fileMapUUID stringByDeletingLastPathComponent];
            NSString * fullFileMapUUIDParent = [dsymMapFolderPath stringByAppendingPathComponent:fileMapUUIDParent];
            if (![fm fileExistsAtPath:fullFileMapUUIDParent]){
                [fm createDirectoryAtPath:fullFileMapUUIDParent withIntermediateDirectories:YES attributes:nil error:nil];
            }
            NSString * linkPath = [dsymMapFolderPath stringByAppendingPathComponent:fileMapUUID];
            NSString * relPath = [buildNumDsymFilePath stringWithPathRelativeTo:fullFileMapUUIDParent];
            if ([fm fileExistsAtPath:linkPath]){
                fmError = nil;
                [fm removeItemAtPath:linkPath error:&fmError];
            }
            [fm createSymbolicLinkAtPath:[dsymMapFolderPath stringByAppendingPathComponent:fileMapUUID]
                     withDestinationPath:relPath error: nil];
            
      
        }
        if ([launchArgs[@"dump-dsym"] boolValue] == YES){
            return;
        }
        if (dumpAway){
        MailDump * dumper = [MailDump new];
        [dumper dumpHeadersToPath:path];
        NSLog (@"🔴🔴🔴🔴🔴🔴🔴🔴🔴🔴🔴🔴🔴🔴🔴🔴");
         

       
        [NSApp activateIgnoringOtherApps:YES];
       
        [[NSUserDefaults standardUserDefaults] setObject:currentOsBuild forKey:@"com.smallcubed.maildump.lastBuild"];
        //  ./class-dump -o /Volumes/Data/Development/MainProjects/SCS/Mail_Headers/DSYMs --dsym  "/System/Applications/Mail.app/Contents/MacOS/Mail"

    
 
        
        [NSApp terminate:nil];
            
        }
       
    }
}
-(void)dumpHeadersToPath:(NSString*)path{
     
    
    CDContext * context = [CDContext new];
 
    NSDictionary * images =
                       @{@"Email"                  : @"/System/Library/PrivateFrameworks/Email.framework/Versions/A/Email",
                         @"EmailDaemon"            : @"/System/Library/PrivateFrameworks/EmailDaemon.framework/Versions/A/EmailDaemon",
                         @"ExchangeWebServices"    : @"/System/Library/PrivateFrameworks/ExchangeWebServices.framework/Versions/A/ExchangeWebServices",
                         @"MailUI"                 : @"/System/Library/PrivateFrameworks/MailUI.framework/Versions/A/MailUI",
                         @"EmailFoundation"        : @"/System/Library/PrivateFrameworks/EmailFoundation.framework/Versions/A/EmailFoundation",
                         @"EmailCore"              : @"/System/Library/PrivateFrameworks/EmailCore.framework/Versions/A/EmailCore",
                         @"MailCore"               : @"/System/Library/PrivateFrameworks/MailCore.framework/Versions/A/MailCore",
                         //@"CoreRecentsFramework"            : @"/System/Library/PrivateFrameworks/CoreRecents.framework/Versions/A/CoreRecents",
                         @"Suggestions"            : @"/System/Library/PrivateFrameworks/Suggestions.framework/Versions/A/Suggestions",
                         @"MailSupport"            : @"/System/Library/PrivateFrameworks/MailSupport.framework/Versions/A/MailSupport",
                         @"MailService"            : @"/System/Library/PrivateFrameworks/MailService.framework/Versions/A/MailService",
                         @"MailApp"                         : @"/System/Applications/Mail.app/Contents/MacOS/Mail",
                         @"EmailAddressing"        : @"/System/Library/PrivateFrameworks/EmailAddressing.framework/Versions/A/EmailAddressing",
                         @"Mail"                   : @"/System/Library/PrivateFrameworks/Mail.framework/Versions/A/Mail",
                         @"IMAP"                   : @"/System/Library/PrivateFrameworks/IMAP.framework/Versions/A/IMAP",
                       //  @"Intents"                         : @"/System/Library/Frameworks/Intents.framework/Versions/A/Intents",
                        // @"IntentsCore"                     : @"/System/Library/PrivateFrameworks/IntentsCore.framework/Versions/A/IntentsCore",
                         //@"CoreSuggestionsFramework"        : @"/System/Library/PrivateFrameworks/CoreSuggestions.framework/Versions/A/CoreSuggestions",
                         
                       };
    
    
    [images enumerateKeysAndObjectsUsingBlock:^(NSString*identifier, NSString * path, BOOL * _Nonnull stop) {
        [context analyseModuleAtPath:path identifier:identifier];
    }];
    NSArray * specificClasses = @[@"ACAccount"];
    [specificClasses enumerateObjectsUsingBlock:^(id  specificClassName, NSUInteger idx, BOOL * _Nonnull stop) {
        Class specificClass = NSClassFromString(specificClassName);
        if (specificClass){
            [CDClass withObjcClass:specificClass inModule:@"Other" context:context];
        }
          
    }];
    
    
    if (!path){
        path = [[@"~/Library/Mail/Headers" stringByStandardizingPath] stringByResolvingSymlinksInPath];
        //path = [path stringByAppendingPathComponent:context.osBuild];
    }
    [context exportToPath:path ];
}

@end
