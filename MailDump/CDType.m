//
//  CDIVar.m
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-10.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import <objc/runtime.h>

#import "CDType.h"
#import "CDParameter.h"
#import "CDContext.h"
#import "NSData+Digest.h"

#import "NSScanner+methodLineScanner.h"



@interface CDType ()

@property (strong) NSString * _rawObjcEncoding_;
@property (assign) NSInteger _pointer_;

@property (weak) CDContext * context;

@end


NSString * stringFromEncodingChar(char encodingChar){
    switch (encodingChar) {
        case '@':
        case '\0': return @"id";
        case '#': return @"Class";
        case ':': return @"SEL";
        case 'c': return @"BOOL";
        case 'C': return @"unsigned char";
        case 's': return @"short";
        case 'S': return @"unsigned short";
        case 'i': return @"int";
        case 'I': return @"unsigned int";
        case 'l': return @"long";
        case 'L': return @"unsigned long";
        case 'q': return @"long long";
        case 'Q': return @"unsigned long long";
        case 'f': return @"float";
        case 'd': return @"double";
        case 'b': return @"bool";
        case 'B': return @"BOOL";
        case 'v': return @"void";
        case '{': return nil;
        case '}': return nil;
        case '[': return nil;
        case ']': return nil;
        default : return nil;
    }
}

@implementation CDType
+(instancetype)typeWithIvarLine:(NSString*)line{
    NSScanner * scanner = [NSScanner scannerWithString:line];
    [scanner setCharactersToBeSkipped:nil];
    CDType * obj;
    NSString * name;
    [scanner scanUpToCharacters:@";" intoCDType:&obj name:&name];
    obj.varName = name;
    return obj;
}
+(instancetype)withObjcEncoding:(NSString*)objcEncoding context:(CDContext*)context{
    CDType * type = context.types[objcEncoding];
    if (!type){
        if ([objcEncoding isEqualToString:@"^@"]){
            
        }
        type = [[CDType alloc] initWithObjcEncoding:objcEncoding context:context];
        context.types[objcEncoding] = type;
//        if (type.declaration == nil){
//            
//        }
    }
    return type;
}

-(CDType*)copy{
    CDType* copy = [CDType new];
    copy.context = self.context;
    copy.attributes = self.attributes;
    copy.typeName = self.typeName;
    copy.protocolNames = self.protocolNames;
    copy.blockParameters = self.blockParameters;
    copy.varName = self.varName;
    copy.structMembers = self.structMembers;
    copy.unionMembers = self.unionMembers;
    copy.arraySize = self.arraySize;
    copy.objcEncoding = self.objcEncoding;
    copy.pointer = self.pointer;
    return copy;
}
-(NSInteger)pointer{
    return self._pointer_;
}
-(void)setPointer:(NSInteger)pointer{
    self._pointer_ = pointer;
}
-(instancetype)initWithObjcEncoding:(NSString*)objcEncoding context:(CDContext*)context{
    self = [self init];
    self._rawObjcEncoding_ = objcEncoding;
    self.attributes = [NSMutableArray new];
    NSUInteger len = objcEncoding.length;
    if ([objcEncoding isEqualToString:@"^@"]){
        
    }
    
    NSScanner * sc = [NSScanner scannerWithString:objcEncoding];
    sc.caseSensitive = YES;
    if ([sc scanString:@"r" intoString:nil]){
        [self.attributes addObject:@"const"];
    }
    if ([sc scanString:@"V" intoString:nil]){
        [self.attributes addObject:@"oneway"];
    }
    if ([sc scanString:@"o" intoString:nil]){
//        [self.attributes addObject:@"out"];
    }
    if ([sc scanString:@"A" intoString:nil]){
        [self.attributes addObject:@"atomic"];
        // unknown type but I have seen it? 
    }
    while ([sc scanString:@"^" intoString:nil]){
        if ([sc scanString:@"?" intoString:nil]){
            NSString * typeName = @"CDUnknownFunctionPointerType";
            self.typeName = self.typeName?[self.typeName stringByAppendingFormat:@" %@",typeName]:typeName;
            return self;
        }
        else{
            self._pointer_++;
        }
    }
    if ([sc scanString:@"*" intoString:nil]){
        self._pointer_++;
        NSString * typeName = @"char";
        self.typeName = self.typeName?[self.typeName stringByAppendingFormat:@" %@",typeName]:typeName;
        return self;
    }
    if ([sc scanString:@"@" intoString:nil]){
        if ([sc scanString:@"?" intoString:nil]){
            NSString * typeName = @"CDUnknownBlockType";
            self.typeName = self.typeName?[self.typeName stringByAppendingFormat:@" %@",typeName]:typeName;
            return self;
        }
        NSString * typeName = @"id";
        if ([sc scanString:@"\"" intoString:nil]){
            NSMutableArray * protocolNames = [NSMutableArray new];
            
            [sc scanUpToCharactersFromString:@"<\"" intoString:&typeName];
            while ([sc scanString:@"<" intoString:nil]){
                NSString* protocolName;
                [sc scanUpToString:@">" intoString:&protocolName];
                if (protocolName) [protocolNames addObject:protocolName];
                [sc scanString:@">" intoString:nil];
            }
            
            [sc scanUpToString:@"\"" intoString:nil];
            [sc scanString:@"\"" intoString:nil];
            
            if (protocolNames.count){
                self.protocolNames = protocolNames;
            }
            if (![typeName isEqualToString:@"id"]){
                self._pointer_++;
            }
        }
        self.typeName = self.typeName?[self.typeName stringByAppendingFormat:@" %@",typeName]:typeName;
        return self;
    }
    if ([sc scanString:@"{" intoString:nil]){
        NSAssert([objcEncoding hasSuffix:@"}"],@"problemo!");
        NSMutableArray * members = [NSMutableArray new];
        NSUInteger idx = sc.scanLocation;
        
        NSRange r = NSMakeRange(idx+0, len-idx-1);
        NSString* structEncodingString =[objcEncoding substringWithRange:r];
        NSRange eqRange = [structEncodingString rangeOfString:@"="];
        if (eqRange.length){
            NSString * typeName = [structEncodingString substringToIndex:eqRange.location];
            if ([typeName isEqualToString:@"?"]){
                NSRange fullRange = NSMakeRange(idx-1, len-idx+1);
                NSString* fullStructEncodingString =[objcEncoding substringWithRange:fullRange];
                NSData * data = [[fullStructEncodingString decomposedStringWithCanonicalMapping] dataUsingEncoding:NSUTF8StringEncoding] ;
                NSString * digest = [data sha1Digest];
                
                NSUInteger length = [digest length];
                if (length > 8){
                    digest = [digest substringFromIndex:length - 8];
                }
                typeName =[NSString stringWithFormat:@"CDStruct_%@", digest];
                if ([typeName isEqualToString:@"CDStruct_627e0f85"]){
                    
                }
            }
            self.typeName = typeName;
            structEncodingString = [structEncodingString substringFromIndex:eqRange.location+1];
            if (structEncodingString.length){
                NSScanner* sc = [NSScanner scannerWithString:structEncodingString];
                sc.caseSensitive = YES;
                while (!sc.isAtEnd){
                    NSString * typeEncoding;
                    NSString * memberName = nil;
                    if ([sc scanString:@"\"" intoString:nil]){
                        [sc scanUpToString:@"\"" intoString:&memberName];
                        [sc scanString:@"\"" intoString:nil];
                    }
                    if ([sc scanObjcTypeEncoding:&typeEncoding]){
                        CDType * member = [[CDType withObjcEncoding:typeEncoding context:context] copy];
                        if (member){
                            member.varName = memberName;
                            if ([member.objcEncoding isEqualToString:@"^@"]){
                                // cannot have pointers to objects in a struct for ARC reasons
                                // make this a pointer to a void
                                member.typeName = @"void";
                            }
                            [members addObject:member];
                        }
                    }
                    else break;
                }
            }
        }
        else{
            NSString * typeName =structEncodingString;
            self.typeName = self.typeName?[self.typeName stringByAppendingFormat:@" %@",typeName]:typeName;
        }
            
        self.structMembers = members;
        return self;
    }
    if ([sc scanString:@"(" intoString:nil]){
        NSAssert([objcEncoding hasSuffix:@")"],@"problemo!");
        NSMutableArray * members = [NSMutableArray new];
        NSUInteger idx = sc.scanLocation;
        NSRange r = NSMakeRange(idx+0, len-idx-1);
        NSString* unionEncodingString =[objcEncoding substringWithRange:r];
        NSRange eqRange = [unionEncodingString rangeOfString:@"="];
        if (eqRange.length){
            NSString * typeName = [unionEncodingString substringToIndex:eqRange.location];
            if ([typeName isEqualToString:@"?"]){
                NSRange fullRange = NSMakeRange(idx-1, len-idx+1);
                NSString* fullUnionEncodingString =[objcEncoding substringWithRange:fullRange];
                NSData * data = [[fullUnionEncodingString decomposedStringWithCanonicalMapping] dataUsingEncoding:NSUTF8StringEncoding] ;
                NSString * digest = [data sha1Digest];
                
                NSUInteger length = [digest length];
                if (length > 8){
                    digest = [digest substringFromIndex:length - 8];
                }
                typeName =[NSString stringWithFormat:@"CDStruct_%@", digest];
            }
            self.typeName = typeName;
            unionEncodingString = [unionEncodingString substringFromIndex:eqRange.location+1];
            if (unionEncodingString.length){
                NSScanner* sc = [NSScanner scannerWithString:unionEncodingString];
                sc.caseSensitive = YES;
                while (!sc.isAtEnd){
                    NSString * typeEncoding;
                    if ([sc scanObjcTypeEncoding:&typeEncoding]){
                        CDType * member = [CDType withObjcEncoding:typeEncoding context:context];
                        
                        if (member){
                            if ([member.objcEncoding isEqualToString:@"^@"]){
                                // cannot have pointers to objects in a struct for ARC reasons
                                // make this a pointer to a void
                                member.typeName = @"void";
                            }
                            [members addObject:member];
                            
                        }
                    }
                    else break;
                }
            }
        }
        else{
            
            NSString * typeName =unionEncodingString;
            self.typeName = self.typeName?[self.typeName stringByAppendingFormat:@" %@",typeName]:typeName;
        }
        
        self.unionMembers = members;
    }
    if ([sc scanString:@"[" intoString:nil]){
        NSAssert([objcEncoding hasSuffix:@"]"],@"problemo!");
        NSUInteger idx = sc.scanLocation;
       
        NSRange r = NSMakeRange(idx+0, len-idx-1);
        
        NSString* arrayEncodingString =[objcEncoding substringWithRange:r];
        NSScanner* sc = [NSScanner scannerWithString:arrayEncodingString];
        sc.caseSensitive = YES;
        NSInteger arraySize;
        [sc scanInteger:&arraySize];
        self.arraySize = arraySize;
        CDType * arrayType = [CDType withObjcEncoding:sc.remainder context:context];
        NSString * typeName =arrayType.typeName;
        self.typeName = self.typeName?[self.typeName stringByAppendingFormat:@" %@",typeName]:typeName;

        self._pointer_ = arrayType._pointer_;
        self.structMembers = arrayType.structMembers;
        return self;
    }
    else {
        NSString * typeName =stringFromEncodingChar(sc.currentCharacter);
        self.typeName = self.typeName?[self.typeName stringByAppendingFormat:@" %@",typeName]:typeName;
    }

    return self;
}

+(instancetype)withPrimitiveObjcEncoding:(unichar)charEncoding{
    CDType  * type = [CDType new];
    
    NSString *encoding = [NSString stringWithCharacters:&charEncoding length:1];
    
    static NSDictionary * typeMap = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        typeMap = @{@"@": @"id",
                    @"#": @"Class",
                    @":": @"SEL",
                    @"c": @"BOOL",
                    @"C": @"unsigned char",
                    @"s": @"short",
                    @"S": @"unsigned short",
                    @"i": @"int",
                    @"I": @"unsigned int",
                    @"l": @"long",
                    @"L": @"unsigned long",
                    @"q": @"long long",
                    @"Q": @"unsigned long long",
                    @"f": @"float",
                    @"d": @"double",
                    @"b": @"bool",
                    @"B": @"NSInteger:1",
                    @"v": @"void",
                    @"*": @"char*"
       };
    });
    NSString * typeName = typeMap[encoding];
    NSAssert(typeName, @"No Type found for encoding %c",charEncoding);
    type.typeName = typeMap[encoding];
    type._rawObjcEncoding_ = encoding;
    return type;
}

-(NSString*)objcEncoding{
    return self._rawObjcEncoding_;
}

-(void)setObjcEncoding:(NSString * _Nonnull)objcEncoding{
    self._rawObjcEncoding_ = objcEncoding;
}

-(CDType*)copyWithZone:(NSZone*)zone{
    CDType * obj = [CDType new];
    obj.varName = [self.varName copy];
    obj.typeName = [self.typeName copy];
    obj.protocolNames = [self.protocolNames copy];
    obj.blockParameters = [self.blockParameters copy];
    return obj;
}
-(NSString*)description{
    return [[super description] stringByAppendingFormat:@"(%@ <%@> %@)",self.typeName,[self.protocolNames componentsJoinedByString:@","], [self.blockParameters valueForKey:@"description"]];
}

-(NSString*)typeDefinition{
    if (self.structMembers){
        NSMutableString * definition = [NSMutableString new];
        if (self.structMembers.count){
            BOOL isAnonymous = [self.typeName hasPrefix:@"CDStruct_"];
            NSString * preMemberName = isAnonymous ? @"" : [self.typeName stringByAppendingString:@" "];
            NSString * postMemberName = isAnonymous ? self.typeName : [@"_" stringByAppendingString:self.typeName];
            
            [definition appendFormat:@"typedef struct %@ {\n",preMemberName];
            [self.structMembers enumerateObjectsUsingBlock:^(CDType * member, NSUInteger idx, BOOL * _Nonnull stop) {
                NSInteger ptrCount = member._pointer_;
                
                NSString * pointers = ptrCount?@" ":nil;
                while (ptrCount--) pointers =[pointers stringByAppendingString:@"*"];
                NSString * typeName = @"";
                if (member.structMembers && member.structMembers.count==0){
                    typeName =@"struct "; // need to refer to a struct
                }
                typeName = [typeName stringByAppendingString:member.typeName];
                
                [definition appendFormat:@"    %@%@ %@%@;\n",
                 typeName,
                 pointers?:@"",
                 member.varName ?: [NSString stringWithFormat:@"_field%ld",idx+1],
                 member.arraySize ?[NSString stringWithFormat:@"[%ld]",member.arraySize]:@""];
            }];
            [definition appendFormat:@"} %@;",postMemberName];
        }
        else{
            [definition appendFormat:@"struct %@;",self.typeName];
        }
      
        return definition;
    }
    return nil;
}
-(NSString*)declaration{
    NSMutableString * decl = [NSMutableString new];
    if (decl){
        if (self.attributes.count){
            [decl appendFormat:@"%@ ",[self.attributes componentsJoinedByString:@" "]];
        }
        if (self.structMembers){
            if ( ![self.typeName hasPrefix:@"CDStruct_"]){
                [decl appendFormat:@"struct "];
            }
        }
        NSString * typeName =self.typeName;
        if (!self.typeName) return nil;
        [decl appendString:typeName];
    
        [decl replaceOccurrencesOfString:@"unsigned long long" withString:@"NSUInteger" options:NSAnchoredSearch range:NSMakeRange(0,decl.length)];
        [decl replaceOccurrencesOfString:@"long long" withString:@"NSInteger" options:NSAnchoredSearch range:NSMakeRange(0,decl.length)];
    
        static NSRegularExpression * arrayDenotionRegEx = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            arrayDenotionRegEx = [NSRegularExpression regularExpressionWithPattern:@"\\[\\d+\\]" options:0 error:nil];
        });
        NSRange arrayRange = [arrayDenotionRegEx rangeOfFirstMatchInString:decl options:0 range:NSMakeRange(0, decl.length)];
        NSString * arrayPart = nil;
        if (arrayRange.length && self.varName){
            arrayPart= [decl substringWithRange:arrayRange];
            [decl replaceCharactersInRange:arrayRange withString:@""];
        }
    
        if (self.protocolNames){
            [decl appendFormat:@" /*<%@>*/",[self.protocolNames componentsJoinedByString:@","]];
        }
        
        if (self.blockParameters){
            [decl appendString:@"(^"];
            if (self.varName)[decl appendString:self.varName];
            [decl appendString:@")("];
            if (self.blockParameters.count){
                [decl appendString:[[self.blockParameters valueForKey:@"blockParameterDeclaration"] componentsJoinedByString:@","]];
            }
            [decl appendString:@")"];
        }
        else if (self.varName){
            [decl appendFormat:@" %@",self.varName];
            if (arrayPart){
                [decl appendString:arrayPart];
            }
        }
        if ([decl hasPrefix:@"<"]){
            [decl insertString:@"id " atIndex:0];
        }
    
        decl = [[decl trimmedString] mutableCopy];
        if (self._pointer_){
            [decl appendString:@" "];
            NSInteger ptrCount = self._pointer_;
            while (ptrCount--){
                [decl appendString:@"*"];
            }
        }
        return decl;
        
    }
    return nil;
}
-(NSString*)objcTypeString{
    NSMutableString * typeString = [NSMutableString new];
    NSString * typeName = self.typeName;
      
    if ([typeName hasSuffix:@"*"]){
        [typeString appendString:@"^"];
        typeName = [typeName stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" *"]];
    }
    if ([typeName hasPrefix:@"_struct "]){
        [typeString appendString:@"{"];
        typeName = [typeString substringFromIndex:@"_struct ".length];
        if ([typeName hasPrefix:@"{"]){
            [typeString appendString:@"?="];
        }
    }
    return nil;
//    @{@"unsigned long long": @"U",
//      @"long long" :@"u",
//
//    }
}

-(NSArray <NSString*> * ) referencedClassNames{
    NSMutableSet * muClasses = [NSMutableSet new];
    if ([self._rawObjcEncoding_ hasPrefix:@"@"]){
        if (![self.typeName isEqualToString:@"id"]){
            [muClasses addObject:self.typeName];
        }
    }
    for (CDParameter * param in self.blockParameters){
        [muClasses addObjectsFromArray:param.referencedClassNames];
    }
   return [muClasses allObjects];
}
-(NSArray <NSString*> * ) referencedProtocolNames{
    NSMutableSet * muProtocols = [NSMutableSet new];
   
    if ([self._rawObjcEncoding_ hasPrefix:@"@"]){
        NSRange openBracket = [self.typeName rangeOfString:@"<"];
        
        if ([self.typeName hasPrefix:@"id"] && openBracket.length ){
            NSString * protocolList = [[self.typeName substringFromIndex:openBracket.location] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
            [muProtocols addObjectsFromArray: [protocolList componentsSeparatedByString:@","]];
        }
    }
    for (NSString * protocol in self.protocolNames){
        [muProtocols addObject:protocol];
    }
    for (CDParameter * param in self.blockParameters){
        [muProtocols addObjectsFromArray:param.referencedProtocolNames];
    }
    return [muProtocols allObjects];
}
-(NSString*)cdClassName{
    
    if ([self.typeName isEqualToString:@"id"]){
        return @"id";
    }
    if (self.pointer==1 && self.structMembers==nil){
        return self.typeName;
    }
    return nil;
}

-(NSString *)basicTypeName{
    NSString * _basicTypeName = self.typeName;
    if ([_basicTypeName hasPrefix: @"struct "]){
        _basicTypeName = [_basicTypeName substringFromIndex:@"struct ".length];
    }
    return [_basicTypeName stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" *"]];
}

-(NSArray <CDType*> *)referencedStructs{
    NSMutableArray * muStructs = [NSMutableArray new];
    if (self.structMembers){
        for (CDType * member in self.structMembers){
            [muStructs addObjectsFromArray:member.referencedStructs];
        }
        [muStructs addObject:self];
    }
    return muStructs;
}
@end
