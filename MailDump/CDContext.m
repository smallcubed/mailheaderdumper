//
//  CDContext.m
//  MailDump
//
//  Created by Scott Morrison on 2020-08-03.
//  Copyright © 2020 smallcubed. All rights reserved.
//

#import "CDContext.h"
#import <objc/runtime.h>
#import "CDClass.h"
#import "CDProtocol.h"
#import "CDType.h"
#import <sys/stat.h>
#import <mach-o/loader.h>

BOOL find_macho(unsigned long addr, unsigned long *base) {
    *base = 0;

    while(1) {
        chmod((char *)addr, 0777);
        if(errno == 2 && /*ENOENT*/
          ((int *)addr)[0] == 0xfeedfacf /*MH_MAGIC_64*/) {
            *base = addr;
            return YES;
        }

        addr += 0x1000;
    }
    return NO;
}

@interface  CDContext()

@end

@implementation CDContext



-(instancetype)init{
    self = [super init];
    if (self){
        self.classes = [NSMutableDictionary new];
        self.types= [NSMutableDictionary new];
        NSDictionary *versionDict = [NSDictionary dictionaryWithContentsOfFile:@"/System/Library/CoreServices/SystemVersion.plist"];
        self.osVersion = [NSString stringWithFormat:@"%@ (%@)",versionDict[@"ProductVersion"], versionDict[@"ProductBuildVersion"]];
        self.osBuild = versionDict[@"ProductBuildVersion"];
        NSDictionary * infoDict = NSBundle.mainBundle.infoDictionary;
        self.mailVersion = [NSString stringWithFormat:@"%@ (%@)", infoDict[@"CFBundleShortVersionString"],infoDict[@"CFBundleVersion"]];
        self.compatibilityUUID   = NSBundle.mainBundle.infoDictionary[@"PluginCompatibilityUUID"];
        self.invalidMethodNames =  @[@"-.cxx_destruct",@"-hash",@"-superclass"];
        self.invalidPropertyNames = @[@"hash",@"superclass"];
        NSLog(@"\n\nOS version: %@ \nMail Version: %@\nCompatibility UUID: %@\n\n-----------------",self.osVersion,self.mailVersion,self.compatibilityUUID);
        
    }
    return self;
    
}

-(CDClass*)addClass:(Class)objcClass forModule:(NSString*)moduleName{
    return [CDClass withObjcClass:objcClass inModule:moduleName context:self];
}

-(NSString*)validStructName:(CDType*)structDef{
    
    NSString * basicStructName =  structDef.basicTypeName;
    if ([basicStructName hasPrefix: @"struct "]){
        basicStructName = [basicStructName substringFromIndex:@"struct ".length];
    }
    NSArray * invalidPrefixes = [ @"NS, _NS, __NS, __CF, CG, __va_list, os_, objc_, CL" componentsSeparatedByString:@", "];
    for (NSString * invalidPrefix in invalidPrefixes){
        if ([basicStructName hasPrefix:invalidPrefix]){
            return nil;
        }
    }
    return basicStructName;
}

-(BOOL)validateMethodName:(NSString*)methodName{
    if ([self.invalidMethodNames containsObject:methodName]){
        return NO;
    }
    return YES;
}
-(BOOL)validatePropertyName:(NSString*)propertyName{
    if ([self.invalidPropertyNames containsObject:propertyName]){
        return NO;
    }
    return YES;
}

-(BOOL)validateClassName:(NSString*)className inModule:(NSString*)imageName{
    if (imageName.length== 0){
        return YES;
    }
    NSArray * invalidPrefixes = [ @"NS,_NS,__NS,__CF,CS,WK,Web,CA,CDUnknown,QLPreview" componentsSeparatedByString:@","];
    if ([imageName isEqualToString:@"MailApp"]){
        invalidPrefixes = [ @"NS,_NS,__NS,__CF,CS,WK,CA,CDUnknown,QLPreview" componentsSeparatedByString:@","];
    }
    for (NSString * invalidPrefix in invalidPrefixes){
        if ([className hasPrefix:invalidPrefix]){
            return NO;
        }
    }
    return YES;
}

-(BOOL)validateProtocolName:(NSString*)protocolName inModule:(NSString*)imageName{
    if (imageName.length== 0){
        return YES;
    }
    NSArray * invalidPrefixes = [ @"NS, _NS, __NS, OS_, QLPreviewItem, QLPreviewPanel, WK, Web, UN, DOM, CS, CNAutocomplete, ABImageClient" componentsSeparatedByString:@", "];
    if ([imageName isEqualToString:@"MailApp"]){
        invalidPrefixes = [ @"NS,_NS,__NS,__CF,CS,WK,CA,CDUnknown,QLPreview" componentsSeparatedByString:@","];
    }
    for (NSString * invalidPrefix in invalidPrefixes){
        if ([protocolName hasPrefix:invalidPrefix]){
            return NO;
        }
    }
    return YES;
}

#define EXECUTABLE_BASE_ADDR 0x100000000

-(void) analyseModuleAtPath:(NSString*)path identifier:(NSString *)identifier{
    unsigned long binary, loadBase;
    if (find_macho(EXECUTABLE_BASE_ADDR, &binary)){
        if(find_macho(binary + 0x1000, &loadBase)){
            struct symtab_command * symtab;
            struct load_command * lc = (struct load_command *)(loadBase + sizeof(struct mach_header_64));
            struct mach_header_64 * mh =((struct mach_header_64 *)loadBase);
            for(int i=0; i<mh->ncmds; i++) {
                if(lc->cmd == 0x2/*LC_SYMTAB*/) {
                    symtab = (struct symtab_command *)lc;
                } else if(lc->cmd == 0x19/*LC_SEGMENT_64*/) {
                    struct segment_command_64 * sc = (struct segment_command_64 * )lc;
                    
                    switch(*((unsigned int *)&sc->segname[2])) { //skip __
                    case 0x4b4e494c:    //LINK
                       // linkedit = (struct segment_command_64 *)lc;
                        break;
                    case 0x54584554:    //TEXT
                        //text = (struct segment_command_64 *)lc;
                        break;
                    }
                }
                                                                                        
                lc = (struct load_command *)((unsigned long)lc + lc->cmdsize);
            }
        }
    }

    unsigned int classCount =0;
    const char * * _classNames = objc_copyClassNamesForImage(path.UTF8String, &classCount);
    unsigned int idx = classCount;
    while (idx--){
        
        const char * _className = _classNames[idx];
        Class class = objc_getClass(_className);
        if (class){
            if ([self validateClassName:[NSString stringWithUTF8String:_className] inModule:identifier]){
                CDClass * cdClass = [CDClass withObjcClass:class inModule:identifier context:self];
                @synchronized (self) {
                    self.classes[cdClass.name] = cdClass;
                }
            }
        }
        
    }
    free (_classNames);
}

-(void)outputProtocolHeadersToPath:(NSString*)path{
    
}



-(void)exportToPath:(NSString *)path{
    NSFileManager * fm = [NSFileManager defaultManager];\
    
    if (![fm fileExistsAtPath:path]){
        [fm createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
   
    NSString * protocolsFolderPath = [path stringByAppendingPathComponent:@"Protocols"];
    if (![fm fileExistsAtPath:protocolsFolderPath]){
        [fm createDirectoryAtPath:protocolsFolderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
   
     [self.protocols enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull protocolName, CDProtocol * _Nonnull protocol, BOOL * _Nonnull stop) {
         if ([self validateProtocolName:protocolName inModule:@""]){
            NSString * fileName = [NSString stringWithFormat:@"%@-Protocol.h",protocolName];
            NSString * filePath = [protocolsFolderPath stringByAppendingPathComponent:fileName];
            
            [protocol.headerFileContents writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        }
    }];
    NSMutableSet * exportedFrameworkPaths = [NSMutableSet new];
    NSMutableArray * exportedClasses = [NSMutableArray new];
    __block NSInteger exportCount = 0;
    [self.classes enumerateKeysAndObjectsUsingBlock:^(NSString * className, CDClass * class, BOOL * _Nonnull stop) {
        if ([self validateClassName:className inModule:class.frameworkName]){
            [exportedClasses addObject:class.name];
            NSString * folderPath = [path stringByAppendingPathComponent:class.frameworkName];
            if (![exportedFrameworkPaths containsObject:folderPath]){
                [exportedFrameworkPaths addObject:folderPath];
                if (![fm fileExistsAtPath:folderPath]){
                    [fm createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
                }
            }
            NSString * fileName = [NSString stringWithFormat:@"%@.h",class.name];
            NSString * filePath = [folderPath stringByAppendingPathComponent:fileName];
           
           
            NSError * writeError = nil;
            if (![class.headerFileContents  writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&writeError]){
                NSLog (@"writing %@ to %@ FAILED",class.className,filePath);
           
            }
            if ((++exportCount)%25){
                NSLog (@"exported %ld classes",exportCount);
            }
            
        }
    }];
  
    NSLog (@"exported %ld classes",exportCount);


    //CDStructs and Common.h
    NSMutableString * contents = [NSMutableString new];
    [contents appendString:@"//\n"];
    [contents appendFormat:@"//   MailCommon.h\n"];
    [contents appendFormat:@"//\n"];
    [contents appendFormat:@"//   OS Version: %@\n",self.osVersion];
    [contents appendFormat:@"//\n"];
    [contents appendFormat:@"//   Mail Version: %@\n",self.mailVersion];
    [contents appendFormat:@"//   Mail UUID: %@\n",self.compatibilityUUID];
    [contents appendString:@"//\n\n"];
    
    [contents appendString:@"#import <Cocoa/Cocoa.h>\n\n"];
  

    NSMutableSet * exportedTypedefs = [NSMutableSet new];
    
    [self.types enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, CDType * typeDef, BOOL * _Nonnull stop) {
        for (CDType * referredType in typeDef.referencedStructs){
            if ([self validStructName:referredType]){
                NSString * typeDef = referredType.typeDefinition;
                if (typeDef && ![exportedTypedefs containsObject:typeDef]){
                    [exportedTypedefs addObject:typeDef];
                    [contents appendFormat:@"%@\n\n",typeDef];
                }
            }
        }
    }];
    NSString * filePath = [path stringByAppendingPathComponent:@"MailTypes.h"];
    NSLog (@"writing MailTypes to %@",filePath);
   
    [contents  writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
}


@end
