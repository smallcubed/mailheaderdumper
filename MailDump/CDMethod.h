//
//  CDMethod.h
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-09.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

NS_ASSUME_NONNULL_BEGIN
@class CDParameter,CDType;
@class CDContext;

@interface CDMethod : NSObject <NSCopying>
@property (strong) NSString * rawDeclaration;
@property (strong) CDType * returnType;
@property (strong) NSArray <CDParameter *> * parameters;
@property (strong) NSString * selector;
@property (readonly) NSString * declaration;

+(instancetype)withObjcClassMethod:(Method)m context:(CDContext*)context;
+(instancetype)withObjcInstanceMethod:(Method)m context:(CDContext*)context;
+(instancetype)withObjcMethodDescription:(struct objc_method_description) md context:(CDContext*) context isInstanceMethod:(BOOL)isInstanceMethod;

-(instancetype)initWithLine:(NSString*)line;
+(CDMethod*)methodWithLine:(NSString*)line;
-(NSArray <NSString*> * ) referencedClassNames;
-(NSArray <NSString*> * ) referencedProtocolNames;
-(NSArray  <CDType*> *) referencedStructs;

@end

NS_ASSUME_NONNULL_END
