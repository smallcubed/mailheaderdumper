//
//  NSObject+ClassDump.m
//  MailDump
//
//  Created by Scott Morrison on 2022-07-14.
//  Copyright © 2022 smallcubed. All rights reserved.
//

#import "NSObject+ClassDump.h"
#import "CDContext.h"
#import "CDClass.h"

@implementation NSObject (ClassDump)
+(NSString*)classDump{
    CDContext * context = [CDContext new];
    CDClass * class = [context addClass:self forModule:nil];
    return [class headerFileContents];
}
@end
