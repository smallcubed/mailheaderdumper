//
//  CDClass.h
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-09.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CDFileObject-Protocol.h"

NS_ASSUME_NONNULL_BEGIN
@class CDMethod,CDProperty,CDType,CDProtocol,CDIVar;
@class CDContext;

@interface CDClass : NSObject <CDFileObject>

@property (copy) NSString * name;

@property (weak) id <CDFileObjectDelegate> delegate;

@property (strong) NSString * superclassName;
@property (strong) CDClass * superCDClass;
@property (strong) NSString * categoryName;
@property (strong) NSString * frameworkName;
@property (strong) NSString * libraryPath;

@property (strong) NSArray <NSString*> * protocolNames;
@property (strong) NSArray <CDProtocol*> * adoptedProtocols;

@property (strong) NSArray <CDMethod*> * methods;
@property (strong) NSArray <CDProperty*> * properties;
@property (strong) NSArray <CDIVar*> * ivarDecls;

@property (strong) NSDictionary <NSString*,NSArray <CDMethod*> *> * legacyMethods;
@property (strong) NSDictionary <NSString*,NSArray <CDProperty*> *> * legacyProperties;


@property (strong) NSString * OSVersion;
@property (strong) NSString * OSBuild;
@property (strong) NSString * dateString;
@property (readonly) NSArray <NSString*> * referencedProtocolNames;
@property (readonly) NSArray <NSString*> * referencedClassNames;

@property (readonly) NSString* headerFileContents;
@property (readonly) NSArray <CDClass*> * ancestry;


+(instancetype)withObjcClass:(Class)objcClass
                     inModule:(NSString*)imageIdentifer
                     context:(CDContext*)context;
-(CDProperty*)superPropertyWithName:(NSString*)name;
-(void)scanInterfaceLine:(NSString*)line;

-(CDProperty*)propertyForName:(NSString*)name;
@end

NS_ASSUME_NONNULL_END
