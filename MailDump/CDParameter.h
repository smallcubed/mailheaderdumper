//
//  CDVariable.h
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-09.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CDType;

@interface CDParameter : NSObject
@property (strong) CDType * type;
@property (strong) NSString * name;
@property (strong) NSArray <NSString*>* attributes;
@property (strong) NSString * comment;

+(instancetype)cdParameterWithDeclaration:(NSString*)declaration;
-(NSString*)methodParameterDeclaration;
-(NSString*)blockParameterDeclaration;
-(NSArray <NSString*> * ) referencedClassNames;
-(NSArray <NSString*> * ) referencedProtocolNames;

@end

NS_ASSUME_NONNULL_END
