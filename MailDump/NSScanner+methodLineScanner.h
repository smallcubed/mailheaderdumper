//
//  NSScanner+methodLineScanner.h
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-09.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import <AppKit/AppKit.h>


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CDType;

@interface NSString (trimming)
-(NSString*)trimmedString;
@end

@interface NSScanner (methodLineScanner)
-(BOOL)scanToClosingParathensisIntoString:(NSString* _Nonnull *_Nullable)aString;
-(BOOL)scanUpToCharacters:(NSString*)stopChars
               intoCDType:(CDType* _Nonnull * _Nullable) byRefType
                     name:(NSString* _Nonnull * _Nullable)name;


-(unichar)currentCharacter;
-(NSString*)remainder;
-(NSUInteger)advance:(NSUInteger)characterCount;
-(NSUInteger)rewind:(NSUInteger)characterCount;
-(BOOL)scanWhiteSpace;
-(unichar)characterAtOffset:(NSInteger)offset;
-(BOOL)scanUpToCharactersFromString:(NSString*)charString intoString:(NSString* _Nonnull *_Nullable)aString;
-(BOOL)scanCharactersFromString:(NSString*)charString intoString:(NSString* _Nonnull *_Nullable)aString;

-(BOOL)scanObjcTypeEncoding:(NSString *_Nonnull *_Nullable)outEncoding;
-(BOOL)scanObjcTypeEncodingIntoCDType:(CDType *_Nonnull *_Nullable)cdType;
-(BOOL)scanMethodSignatureIntoCDTypes:(NSArray <CDType*> *_Nonnull *_Nullable)cdTypes;

@end

NS_ASSUME_NONNULL_END
