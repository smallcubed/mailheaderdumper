//
//  CDContext.h
//  MailDump
//
//  Created by Scott Morrison on 2020-08-03.
//  Copyright © 2020 smallcubed. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CDProtocol;
@class CDClass;
@class CDType;
@interface CDContext : NSObject
@property (strong) NSMutableDictionary <NSString* ,CDClass *> * classes;
@property (strong) NSMutableDictionary <NSString* ,CDProtocol *> * protocols;
@property (strong) NSMutableDictionary <NSString* ,CDType *> * types;

@property (strong) NSArray <NSString*> * invalidPropertyNames;
@property (strong) NSArray <NSString*> * invalidMethodNames;
@property (strong) NSString * exportPath;

@property (strong) NSString * osVersion;
@property (strong) NSString * osBuild;
@property (strong) NSString * mailVersion;
@property (strong) NSString * compatibilityUUID;

-(void)analyseModuleAtPath:(NSString*)path identifier:(NSString *)identifier;
-(BOOL)validateMethodName:(NSString*)methodName;
-(BOOL)validatePropertyName:(NSString*)propertyName;
-(BOOL)validateClassName:(NSString*)className inModule:(nullable NSString*)moduleName;
-(BOOL)validateProtocolName:(NSString*)protocolName inModule:(nullable NSString*)moduleName;
-(NSString*)validStructName:(CDType*)structDef;

-(void)recordStructs:(NSArray <CDType*> *)structs;

-(void)exportToPath:(NSString*)path;
-(CDClass*)addClass:(Class)objcClass forModule:(nullable NSString*)moduleName;

@end

NS_ASSUME_NONNULL_END
