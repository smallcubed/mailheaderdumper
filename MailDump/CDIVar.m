//
//  CDIVar.m
//  MailDump
//
//  Created by Scott Morrison on 2020-08-06.
//  Copyright © 2020 smallcubed. All rights reserved.
//

#import "CDIVar.h"
#import "CDType.h"

@implementation CDIVar

-(NSString*)declaration{
    return [NSString stringWithFormat:@"      // %@ %@ // %ld",self.type.declaration, self.name, self.offset];
}
@end
