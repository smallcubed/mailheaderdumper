//
//  NSData+SHA1.m
//  dsdump
//
//  Created by Scott Morrison on 2020-07-27.
//  Copyright © 2020 Selander. All rights reserved.
//

#import "NSData+Digest.h"
#import <CommonCrypto/CommonDigest.h>


@implementation  NSData (Digest)
- (NSString *)sha1Digest{
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(self.bytes, (CC_LONG)self.length, digest);

    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++){
        [output appendFormat:@"%02x", digest[i]];
    }
    return output;
}
@end
