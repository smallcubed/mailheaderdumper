//
//  CDProperty.m
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-09.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import "CDProperty.h"
#import "CDParameter.h"
#import "CDType.h"
#import <objc/runtime.h>
#import "CDContext.h"

#import "NSScanner+methodLineScanner.h"
@interface CDProperty()
@property (strong) NSString * _getterName_;
@property (strong) NSString * _setterName_;
@property (weak) CDContext * context;

@end;

@implementation CDProperty
+(instancetype)withObjcProperty:(objc_property_t)objcProperty context:(CDContext*)context{
    CDProperty * prop = [CDProperty new];
    
    const char * _propertyName = property_getName(objcProperty);
    if (_propertyName){
        prop.name = [NSString stringWithUTF8String:_propertyName];
    }

    unsigned int attrCount = 0;
    NSMutableArray * attributes = [NSMutableArray new];
    prop.type = [CDType withPrimitiveObjcEncoding:'@'];
    
    objc_property_attribute_t * rawAttributes = property_copyAttributeList(objcProperty, &attrCount);
    NSString * storageMethod = nil;
  if (rawAttributes){
        while (attrCount--){
            objc_property_attribute_t attr = rawAttributes[attrCount];
            if (attr.name[0] == 'T'){
                prop.type = [CDType withObjcEncoding:[NSString stringWithUTF8String:attr.value] context:context];
            }
            else if (attr.name[0] == 'R'){
                storageMethod = @"readonly";
            }
            else if (attr.name[0] == '&'){
                storageMethod = storageMethod?:@"strong";
            }
            else if (attr.name[0] == 'N'){
                [attributes addObject:@"nonatomic"];
            }
            else if (attr.name[0] == 'C'){
                storageMethod = storageMethod?[storageMethod stringByAppendingString:@",copy"]:@"copy";
            }
            else if (attr.name[0] == 'W'){
                storageMethod = storageMethod?:@"weak";
            }
            else if (attr.name[0] == 'S'){
                prop._setterName_ = [NSString stringWithUTF8String:attr.value];
            }
            else if (attr.name[0] == 'G'){
                prop._getterName_ = [NSString stringWithUTF8String:attr.value];
            }
        }
       free(rawAttributes);
    }
    [attributes addObject:storageMethod?:@"assign"];
    if ([attributes containsObject:@"readonly"]){
        [attributes removeObject:@"nonatomic"];
    }
    
    prop.attributes = attributes;
    return prop;
    
}
-(instancetype)initWithLine:(NSString*)line{
    //@property(readonly, nonatomic) long long databaseID; // @synthesize databaseID=_databaseID;
    self =[self init];
    if (self){
        NSCharacterSet * whiteSpaceCS = [NSCharacterSet whitespaceCharacterSet];
        NSScanner * scanner = [NSScanner scannerWithString:line];
        [scanner setCharactersToBeSkipped:nil];
        if ([scanner scanString:@"@property" intoString:nil]){
            [scanner scanWhiteSpace];
            if (scanner.currentCharacter =='('){
                [scanner advance:1];
                NSString * attributes = nil;
                if ([scanner scanUpToString:@")" intoString:&attributes]){
                    [scanner scanString:@")" intoString:nil];
                    
                    NSMutableArray * muAttr = [NSMutableArray new];
                    NSArray * rawAttr =  [attributes componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@", "]];
                    [rawAttr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        NSString * attr = [obj stringByTrimmingCharactersInSet:whiteSpaceCS];
                        
                        if (attr.length){
                            [muAttr addObject:attr];
                        }
                    }];
                    self.attributes = [muAttr copy];
                }
                [scanner scanWhiteSpace];
                
            }
            else{ // no attributes means assign
                self.attributes = @[@"assign"];
            }
            CDType * propertyType ;
            NSString * propertyName;
            if ([scanner scanUpToCharacters:@";" intoCDType:&propertyType name:&propertyName]){
                if ([propertyType.typeName hasPrefix:@"__weak "]){
                    propertyType.typeName = [propertyType.typeName stringByReplacingOccurrencesOfString:@"__weak " withString:@""];
                    NSMutableArray * muAttr = [self.attributes mutableCopy];

                    if ([self.attributes containsObject:@"assign"]){
                        [muAttr removeObject:@"assign"];
                    }
                    if ([self.attributes containsObject:@"strong"]){
                        [muAttr removeObject:@"strong"];
                    }
                    [muAttr addObject:@"weak"];
                    self.attributes = [muAttr copy];
                }
                
                self.type =propertyType;
                self.name = propertyName;
                [scanner advance:1];
            }
            [scanner scanUpToString:@"//" intoString:nil];
            if ([scanner scanString:@"//" intoString:nil]){
                self.comment = [[scanner remainder] stringByTrimmingCharactersInSet:whiteSpaceCS];
            }
        }
    }
    return self;
}
-(CDProperty*)copyWithZone:(NSZone*)zone{
    CDProperty * prop = [CDProperty new];
    prop.name = [self.name copy];
    prop.attributes = [self.attributes copy];
    prop.type = [self.type copy];
    return prop;
}
+(CDProperty*)propertyWithLine:(NSString*)line{
    CDProperty * prop = [[CDProperty alloc] initWithLine:line];
    return prop;
    
}
-(NSString*)description{
    return [[super description] stringByAppendingFormat:@" - %@ ",self.name];
}
-(NSString*)getterName{
    if (self._getterName_){ return self._getterName_;}
    else return self.name;
}
   
-(NSString*)setterName{
    if (self._setterName_){ return self._setterName_;}
    NSString * firstChar = [[self.name substringToIndex:1] uppercaseString];
    NSString * remainder = [self.name substringFromIndex:1];
    return [NSString stringWithFormat:@"set%@%@:",firstChar,remainder];
}

-(NSArray <NSString*> * ) referencedClassNames{
    NSMutableSet * muClasses = [NSMutableSet new];
    [muClasses addObjectsFromArray:[self.type referencedClassNames]];
    return [muClasses allObjects];
}
-(NSArray <NSString*> * ) referencedProtocolNames{
    NSMutableSet * muProtocols = [NSMutableSet new];
    [muProtocols addObjectsFromArray:[self.type referencedProtocolNames]];
    return [muProtocols allObjects];
}

-(NSArray  <CDType*> *) referencedStructs{
    NSMutableSet <NSString*> * structNames = [NSMutableSet new];
    NSMutableArray <CDType*> * structs = [NSMutableArray new];
    
    for (CDType * structMember in self.type.referencedStructs){
        if (![structNames containsObject:structMember.typeName]){
            [structs addObject:structMember];
            [structNames addObject:structMember.typeName];
        }
    }
    return structs;
}

-(NSString*)declaration{
    NSMutableString * contents = [NSMutableString new];
    [contents appendFormat:@"@property "];
    if (self.attributes.count){
        NSMutableArray * muAttributes = [NSMutableArray new];
        [self.attributes enumerateObjectsUsingBlock:^(NSString * attribute, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([attribute isEqualToString:@"assign"]){
                if ([self.type.typeName hasSuffix:@"*"]){
                    [muAttributes addObject:@"weak"];
                }
                else{
                    [muAttributes addObject:attribute];
                }
            }
            else [muAttributes addObject:attribute];
        }];
        if (self._getterName_){
            [muAttributes addObject:[NSString stringWithFormat:@"getter=%@",self._getterName_]];
        }
        if (self._setterName_){
            [muAttributes addObject:[NSString stringWithFormat:@"setter=%@",self._setterName_]];
        }
        
        [contents appendFormat:@"(%@) ",[muAttributes componentsJoinedByString:@","]];
    }
    [contents appendFormat:@"%@ %@;",self.type.declaration,self.name];
    return contents;
}

@end
