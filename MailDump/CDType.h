//
//  CDIVar.h
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-10.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CDParameter;
@class CDContext;

@interface CDType : NSObject
@property (strong) NSMutableArray <NSString*>* attributes; // const oneway etc
@property (strong) NSString * typeName; // also returnType of Block
@property (strong) NSArray <NSString*>* protocolNames; // all protocols (foound in type and paramType
@property (strong) NSArray <CDParameter *> * blockParameters; //if Block
@property (strong) NSString * varName;
@property (readonly) NSString * cdClassName;
@property (readonly) NSString * basicTypeName; //name removing pointer
@property (strong) NSArray <CDType*> * structMembers; // nil denotes not a struct,  @[] denotes a struct with no members;
@property (strong) NSArray <CDType*> * unionMembers; // nil denotes not a struct,  @[] denotes a struct with no members;

@property (assign) NSInteger arraySize; // 0 denotes not an array;
@property (readonly) NSString * typeDefinition;
@property (strong) NSString * objcEncoding;
@property (assign) NSInteger pointer;



+(instancetype)typeWithIvarLine:(NSString*)line;
+(instancetype)withPrimitiveObjcEncoding:(unichar)charEncoding;
+(instancetype)withObjcEncoding:(NSString*)objcEncoding context:(CDContext*)context;

-(NSString*)declaration;
-(NSArray <NSString*> * ) referencedClassNames;
-(NSArray <NSString*> * ) referencedProtocolNames;
-(NSArray <CDType*> *)referencedStructs;
@end


NS_ASSUME_NONNULL_END
