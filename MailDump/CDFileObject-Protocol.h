//
//  CDFileObject-Protocol.h
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-16.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class CDProtocol,CDClass,CDMethod,CDProperty;

@protocol CDFileObjectDelegate <NSObject>
-(CDProtocol*)protocolForName:(NSString*)protocolName;
-(CDClass*)classForName:(NSString*)className;
@end


@protocol CDFileObject <NSObject>
@property (weak) id <CDFileObjectDelegate> delegate;
@property (strong) NSString * name;

@property (strong) NSArray <NSString*> * protocolNames;
@property (readonly) NSArray <CDProtocol*> * adoptedProtocols;
@property (strong) NSArray <CDMethod*> * methods;
@property (strong) NSArray <CDProperty*> * properties;
@property (strong) NSString * OSVersion;
@property (strong) NSString * OSBuild;
@property (strong) NSString * dateString;
@property (readonly) NSArray <NSString*> * referencedProtocolNames;
@property (readonly) NSArray <NSString*> * referencedClassNames;
@property (readonly) NSString* pathInProject;


-(void)importHeaderFileAtPath:(NSString*)path;

-(NSSet*)adoptProtocolsInMap:(NSDictionary*)protocolMap;



@end

NS_ASSUME_NONNULL_END
