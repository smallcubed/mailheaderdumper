//
//  CDIVar.h
//  MailDump
//
//  Created by Scott Morrison on 2020-08-06.
//  Copyright © 2020 smallcubed. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CDType;

NS_ASSUME_NONNULL_BEGIN

@interface CDIVar : NSObject
@property (strong) NSString * name;
@property (assign) NSInteger offset;
@property (strong) CDType * type;

-(NSString*)declaration;
@end

NS_ASSUME_NONNULL_END
