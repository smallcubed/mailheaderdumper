//
//  CDClass.m
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-09.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import "CDClass.h"
#import "CDMethod.h"
#import "CDParameter.h"
#import "CDProperty.h"
#import "CDType.h"
#import "CDProtocol.h"
#import "CDIVar.h"
#import "CDContext.h"

#import "NSScanner+methodLineScanner.h"
#import <objc/runtime.h>

@interface CDClass ()
@property (weak) CDContext * context;
@property (weak) Class objcClass;
@property (strong) NSMutableDictionary <NSString*,CDProperty*> * _propertiesByName_;
@property (copy) NSString * _name_;
@end

@implementation CDClass

+(instancetype)withObjcClass:(Class)objcClass inModule:(NSString*) moduleName context:(CDContext*)context{
    const char* _className = class_getName(objcClass);
    NSString * className = [NSString stringWithUTF8String:_className];
    @synchronized (context) {
        CDClass * existingClass = context.classes[className];
        if (existingClass){
            if ([existingClass.frameworkName isEqualToString:@"Other"] || existingClass.frameworkName == nil){
                existingClass.frameworkName = moduleName;
            }
            return existingClass;
        }
    }
    
    if ([context validateClassName: className inModule:moduleName]){
        @synchronized (context) {
            context.classes[className] = [CDClass new]; // placeHolder
        }
        CDClass * class = [[self alloc] initWithObjcClass:objcClass inModule:  moduleName context:context];
        @synchronized (context) {
            context.classes[className] = class;
        }
        return class;
    }
    return nil;
    
}

-(void)setName:(NSString *)name{
    self._name_ =  [name stringByReplacingOccurrencesOfString:@"." withString:@"_"];
}
-(NSString *)name{
    return self._name_;
}

-(instancetype)initWithObjcClass:(Class)objcClass inModule:(NSString*) moduleName context:(CDContext*)context{
    self = [self init];

    self.context = context;
    self.objcClass = objcClass;
    self._propertiesByName_ = [NSMutableDictionary new];
    self.frameworkName  = moduleName;
    const char * _libraryPath = class_getImageName(objcClass);
    self.libraryPath =_libraryPath?[NSString stringWithUTF8String:_libraryPath]:nil;
    
    const char* _className = class_getName(objcClass);
    NSString * className = [NSString stringWithUTF8String:_className];
    self.name = className;
    
    if ([className isEqualToString:@"MailApp.MessageListColumnConfigurationMigrator"]){
        
    }
    Class objcSuperclass = class_getSuperclass(objcClass);
    if (objcSuperclass){
        CDClass * cdSuperClass =[CDClass withObjcClass:objcSuperclass inModule:@"Other" context:context];
        if (cdSuperClass){
            self.superclassName = cdSuperClass.name;
            self.superCDClass = cdSuperClass;
        }
        else{
            self.superclassName = NSStringFromClass(objcSuperclass);
        }
    }
    
    self.adoptedProtocols = @[];
    {// protocols
        NSMutableArray * cdProtocols = [NSMutableArray new];
        
        unsigned int protoCount = 0;
        Protocol * __unsafe_unretained * protocols = class_copyProtocolList(objcClass, &protoCount);
        while (protoCount--){
            Protocol * p  = protocols[protoCount];
            CDProtocol * cdProtocol = [CDProtocol withObjcProtocol:p moduleName:moduleName context:context];
            if (cdProtocol){
                [cdProtocols addObject:cdProtocol];
            }
        }
        if (protocols) free(protocols);
        self.adoptedProtocols = cdProtocols;
    }
    
    { //ivars
        unsigned int ivarCount = 0;
        
        Ivar * objcIVars = class_copyIvarList(objcClass, &ivarCount);
        NSMutableArray <CDIVar* > *ivars = [NSMutableArray new];
        while (ivarCount--){
            Ivar  objcIvar = objcIVars[ivarCount];
            const char * _encoding = ivar_getTypeEncoding(objcIvar);
            const char * _name = ivar_getName(objcIvar);
            ptrdiff_t offset = ivar_getOffset(objcIvar);
            if (_encoding && _name){
                NSString * encoding = [NSString stringWithUTF8String:_encoding];
                NSString * name = [NSString stringWithUTF8String:_name];
                CDType * type = [CDType withObjcEncoding:encoding context:nil];
                CDIVar * ivar = [CDIVar new];
                ivar.name = name;
                ivar.type = type;
                ivar.offset = (NSUInteger)offset;
                [ivars insertObject:ivar atIndex:0];
            }
        }
        if (objcIVars){
            free(objcIVars);
        }
        self.ivarDecls = ivars;
    }
    
    { // properties
        unsigned int propertyCount = 0;
        NSMutableArray * cdProperties = [NSMutableArray new];
        objc_property_t * objcProperties = class_copyPropertyList(objcClass, &propertyCount);
        NSInteger idx = propertyCount;
        while (idx--){
            objc_property_t p = objcProperties[idx];
            NSString * propName = [NSString stringWithUTF8String:property_getName(p)];
            if ([context validatePropertyName:propName]){
                CDProperty * prop = [CDProperty withObjcProperty:p context:context];
            
                if (!self._propertiesByName_[prop.name]){
                    if ([self.name isEqualToString:@"RichMessageChildCellView"]&&
                        [propName isEqualToString:@"description"]){
                        
                    }
                    CDProperty * superProp = [self superPropertyWithName:propName];
                    if (superProp){
                        if (![superProp.attributes isEqualToArray:prop.attributes]){
                            prop.attributes = [superProp.attributes copy];
                        }
                    }
                    else{
                        CDProperty * protoProp = [self protocolPropertyWithName:propName];
                        if (protoProp && ![protoProp.attributes isEqualToArray:prop.attributes]){
                            prop.attributes = [protoProp.attributes copy];
                        }
                    }
                    
                    [cdProperties addObject:prop];
                    self._propertiesByName_[prop.name] = prop;
                }
                else{
                    NSLog (@"%@ property %@ duplicates: %@",self.name, prop.declaration,self._propertiesByName_[prop.name].declaration);
                }
            }
        }
        if (objcProperties) free (objcProperties);
        self.properties = cdProperties;
    }
    NSMutableArray* cdMethods = [NSMutableArray new];
    NSMutableSet * addedMethods = [NSMutableSet new];
          
    { // classMethods
        unsigned int methodCount = 0;
        Method * methods = class_copyMethodList(objc_getMetaClass(_className), &methodCount);
        while (methodCount--){
            Method m = methods[methodCount];
            CDMethod * method = [CDMethod withObjcClassMethod:m context:context];
            if ([context validateMethodName:method.selector]){
                if (![addedMethods containsObject:method.selector]){
                    [cdMethods addObject:method];
                    [addedMethods addObject:method.selector];
                }
               // [context recordStructs:method.referencedStructs];
            }
        }
        if (methods) free (methods);
    }
    {// instanceMethods
        unsigned int methodCount = 0;
        Method * methods = class_copyMethodList(objcClass, &methodCount);
        while (methodCount--){
            Method m = methods[methodCount];
            CDMethod * method = [CDMethod withObjcInstanceMethod:m context:context];
            if ([context validateMethodName:method.selector]){
                if (![addedMethods containsObject:method.selector]){
                    [cdMethods addObject:method];
                    [addedMethods addObject:method.selector];
                }
               // [context recordStructs:method.referencedStructs];
            }
        }
        if (methods) free (methods);
        self.methods = cdMethods;
    }
  
    return self;
}
-(CDProperty*)propertyForName:(NSString*)name{
    return self._propertiesByName_[name];
}
-(instancetype)init{
    self = [super init];
    if (self){
        
        NSDateFormatter * df = [[NSDateFormatter alloc] init];
        [df setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
        [df setDateStyle:NSDateFormatterMediumStyle];
        [df setTimeStyle:NSDateFormatterNoStyle];
        
        self.dateString  = [df stringFromDate:[NSDate date]];
    }
    return self;
    
}

-(void)importHeaderFileAtPath:(NSString*)path{
NSError * fileReadError= nil;
NSString * fileContents = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&fileReadError] ;
    [self importFileContents:fileContents];
}




-(void)importFileContents:(NSString*)fileContents{
  
    NSMutableArray * properties = [NSMutableArray new];
    NSMutableArray * methods = [NSMutableArray new];
    NSMutableArray * ivars = [NSMutableArray new];
    NSMutableArray * uncollectedLines = [NSMutableArray new];
    NSMutableArray * imports = [NSMutableArray new];
    NSMutableArray * protocolDeclarations = [NSMutableArray new];
    NSMutableArray * classDeclarations = [NSMutableArray new];
    
    __block BOOL collectingIvars = NO;
    __block BOOL buildingClass = NO;
    [fileContents enumerateLinesUsingBlock:^(NSString *aLine, BOOL *stop) {
        if ([aLine hasPrefix:@"@interface"]){
            [self scanInterfaceLine:aLine];
            buildingClass=YES;
        }
        if ([aLine hasPrefix:@"@end"]){
            buildingClass = NO;
            self.properties = properties;
            self.methods = methods;
            self.ivarDecls = ivars;
        }
        else if (buildingClass){
            if ([aLine hasPrefix:@"@property"]){
                CDProperty * property = [CDProperty propertyWithLine:aLine];
                [properties addObject:property];
            }
            else if ([aLine hasPrefix:@"-"]){
                CDMethod * method = [CDMethod methodWithLine:aLine];
                [methods addObject:method];
            }
            else if ([aLine hasPrefix:@"+"]){
                CDMethod * method = [CDMethod methodWithLine:aLine];
                [methods addObject:method];
            }
            else if ([aLine hasPrefix:@"{"]){
                collectingIvars = YES;
            }
            else if ([aLine hasPrefix:@"}"]){
                collectingIvars = NO;
            }
            else if (collectingIvars){
               CDType * ivar = [CDType typeWithIvarLine:aLine];
                if (ivar){
                    [ivars addObject:ivar];
                }
            }
        }
        else if ([aLine hasPrefix:@"#import"]){
            [imports addObject:aLine];
        }
        else if ([aLine hasPrefix:@"@class"]){
            [classDeclarations addObject:aLine];
        }
        else if ([aLine hasPrefix:@"@protocol"] && [aLine hasSuffix:@";"]){
            [protocolDeclarations addObject:aLine];
        }
        else if (aLine.length>0){
            [uncollectedLines addObject:aLine];
        }
    }];
}
-(void)scanInterfaceLine:(NSString*)line{
    //@interface MFLibrary : NSObject <EDSearchableIndexReasonProvider, MCActivityTarget>
    //@interface ABAddressBookPersion (MailAdditions) <EDSearchableIndexReasonProvider, MCActivityTarget>
    
    
    NSCharacterSet * whiteSpace = [NSCharacterSet whitespaceCharacterSet];
    NSScanner * scanner = [NSScanner scannerWithString:[line stringByTrimmingCharactersInSet:whiteSpace]];
    [scanner setCharactersToBeSkipped:nil];
    if ([scanner scanString:@"@interface " intoString:nil]){
        NSString * className;
        if ([scanner scanUpToCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:@"(:"]
                                    intoString:&className]){
            self.name = [className stringByTrimmingCharactersInSet:whiteSpace];
            if ([scanner scanString:@"(" intoString:nil]){
                NSString * categoryName = nil;
                [scanner scanUpToString:@")" intoString:&categoryName];
                self.categoryName = [categoryName stringByTrimmingCharactersInSet:whiteSpace];
            }
            else if ([scanner scanString:@":" intoString:nil]){
                
            }
                
        }
        NSString * superclassName;
        if ([scanner scanUpToString:@"<" intoString:&superclassName]){
            if (!self.categoryName){
                self.superclassName =[superclassName stringByTrimmingCharactersInSet:whiteSpace];
            }
            [scanner scanString:@"<" intoString:nil];
            NSString * protocolList;
            if ([scanner scanUpToString:@">" intoString:&protocolList]){
                [scanner scanString:@">" intoString:nil];
                NSArray * rawProtocols = [protocolList componentsSeparatedByString:@","];
                NSMutableArray * muProtocols = [NSMutableArray new];
                for (NSString * rawProtocol in rawProtocols){
                    NSString * protocol = [rawProtocol stringByTrimmingCharactersInSet:whiteSpace];
                    if (protocol.length){
                        [muProtocols addObject:protocol];
                    }
                }
                self.protocolNames = [muProtocols copy];;
            }
        }
        else{
            self.superclassName = [[scanner remainder]stringByTrimmingCharactersInSet:whiteSpace];
        }
    }
    
}

-(NSArray <NSString*> * )referencedProtocolsInMethods:(NSArray*)methods
                                           properties:(NSArray*)properties{
    return nil;
}


-(NSArray <NSString*> * )referencedProtocolNames{
    NSMutableSet * muProtocols = [NSMutableSet set];
    [muProtocols addObjectsFromArray:self.protocolNames];
    for (CDProperty * prop in self.properties){
        [muProtocols addObjectsFromArray:prop.referencedProtocolNames];
    }
    for (CDMethod * method in self.methods){
        [muProtocols addObjectsFromArray:method.referencedProtocolNames];
    }
 
    return [muProtocols allObjects];
}

-(CDProperty*)protocolPropertyWithName:(NSString*)name{
   for (CDProtocol* protocol in self.adoptedProtocols){
        for (CDProtocol * ancestorProto in protocol.ancestry){
            for (CDProperty * prop in ancestorProto.properties){
                if ([prop.name isEqualToString:name]){
                    return prop;
                }
            }
        }
    }
    return nil;
}

-(CDProperty*)superPropertyWithName:(NSString*)name{
    CDProperty * returnProp = nil;
    CDClass * supClass = self.superCDClass;
    if ([self.name isEqualToString:@"MessageListCell"]){
        if ([name isEqualToString:@"stringValue"]){
            
        }
    }
    while (supClass && ! returnProp){
        returnProp = [supClass propertyForName:name];
        if (!returnProp){
            supClass = [supClass superCDClass];
        }
    }
    if (!returnProp){
        Class appKitSuper = NSClassFromString(self.superclassName);
        
        objc_property_t *appKitSuperProperties = nil;
        while (appKitSuper && ! returnProp){
            unsigned int propIdx = 0;
            appKitSuperProperties = class_copyPropertyList(appKitSuper, &propIdx);
            while (propIdx--){
                objc_property_t akProp =appKitSuperProperties[propIdx];
                const char * _propName = property_getName(akProp);
                NSString *propName = _propName?[NSString stringWithUTF8String:_propName]:nil;
                if ([propName isEqualToString:@"stringValue"]){
                           
                       }
                if ([propName isEqualToString:name]){
                    returnProp = [CDProperty withObjcProperty:akProp context:self.context];
                    break;
                }
            }
            if (appKitSuperProperties){
                free(appKitSuperProperties);
            }
            if (!returnProp){
                appKitSuper = class_getSuperclass(appKitSuper);
            }
        }
    }
    return returnProp;
    
}
-(CDProperty*)_superPropertyWithName:(NSString*)name{
    CDProperty * returnProp = nil;
    if (self.superCDClass){
        returnProp = [self.superCDClass _superPropertyWithName:name];
    }
   
  
    if (!returnProp){
        for (CDProperty * prop in self.properties){
            if ([prop.name isEqualToString:name]){
                returnProp =  prop;
            }
        }
    }
    return returnProp;
}

-(NSString*) pathInProject{
    return [NSString stringWithFormat:@"%@/%@.h",self.frameworkName,self.name];
}

-(NSArray <NSString*> * ) referencedClassNames{
    NSMutableSet * muClasses = [NSMutableSet set];
    for (CDProperty * prop in self.properties){
        NSArray * classes = prop.referencedClassNames;
        [muClasses addObjectsFromArray:classes];
    }
    for (CDMethod * method in self.methods){
        NSArray * classes = method.referencedClassNames;
        [muClasses addObjectsFromArray: classes];
    }
   
     return [muClasses allObjects];
}
-(NSString*)description{
    return [[super description] stringByAppendingFormat:@"@interface %@ : %@ ",self.name, self.superclassName];
}




-(NSString*)headerFileContents{
    NSMutableString * contents = [NSMutableString new];
    
    [contents appendString:@"//\n"];
    [contents appendFormat:@"//   %@.h\n",self.name];
    if (self.libraryPath){
        [contents appendFormat:@"//        %@\n",self.libraryPath];
    }
    [contents appendFormat:@"//\n"];
    [contents appendFormat:@"//   OS Version: %@\n",self.context.osVersion];
    [contents appendFormat:@"//\n"];
    [contents appendFormat:@"//   Mail Version: %@\n",self.context.mailVersion];
    [contents appendFormat:@"//   Mail UUID: %@\n",self.context.compatibilityUUID];
   
    [contents appendString:@"//\n\n"];

    [contents appendString:@"#import \"MailCommon.h\"\n\n"];
    [contents appendString:@"\n"];
    [contents appendString:@"#pragma clang diagnostic push\n"];
    [contents appendString:@"#pragma clang diagnostic ignored \"-Wdeprecated-declarations\"\n"];
    NSMutableArray * muClassesToDeclare = [NSMutableArray new];
    NSMutableArray * muClassesToImport = [NSMutableArray new];
    
    if (self.superclassName && [self.context validateClassName:self.superclassName inModule:self.frameworkName]){
        
        [muClassesToImport addObject:self.superclassName];
    }
    else{
        NSLog(@"could validate superclass for %@",self);
    }
    // go through all the properties looking for overrides
    NSArray * ancestry = [self ancestry];
    [muClassesToDeclare addObjectsFromArray:self.referencedClassNames];
    
    [self.properties  enumerateObjectsUsingBlock:^(CDProperty * prop, NSUInteger idx, BOOL * _Nonnull stop) {
        CDProperty * superProp = [self superPropertyWithName:prop.name];
        NSString * superPropertyClass = superProp.type.cdClassName;
        NSString * myPropertyClass = prop.type.cdClassName;
        if ([self.name isEqualToString:@"MessageViewController"] && [prop.name isEqualToString:@"view"]){
            
        }
        if (superPropertyClass && myPropertyClass){
            if (![superPropertyClass isEqualToString: myPropertyClass]){
                [muClassesToImport addObject:myPropertyClass];
                [muClassesToDeclare removeObject:myPropertyClass];
            }
        }
    }];
    
    if (muClassesToImport.count){
        [contents appendString:@"\n"];
        [contents appendString:@"// - Import Classes -\n"];
        [muClassesToImport sortUsingSelector:@selector(caseInsensitiveCompare:)];
        [muClassesToImport enumerateObjectsUsingBlock:^(NSString * className, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self.context validateClassName:className inModule:self.frameworkName]){
                [contents appendFormat:@"#import \"%@.h\"\n",className];
            }
        }];
    }
    
    if (muClassesToDeclare.count){
        [contents appendString:@"\n"];
        [contents appendString:@"// - Declared Classes -\n"];
        [muClassesToDeclare sortUsingSelector:@selector(caseInsensitiveCompare:)];
       
        [muClassesToDeclare enumerateObjectsUsingBlock:^(NSString * className, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([self.context validateClassName:className inModule:self.frameworkName]){
                [contents appendFormat:@"@class %@;\n",className];
            }
        }];
    }
    
    NSMutableSet * adoptedProtocolNames = [NSMutableSet new];
    if (self.adoptedProtocols.count){
        NSMutableString * muProtocolsImports = [NSMutableString new];
        for (CDProtocol * p in self.adoptedProtocols){
            if ([self.context validateProtocolName:p.name inModule:self.frameworkName]){
                [adoptedProtocolNames addObject:p.name];
                [muProtocolsImports appendFormat:@"#import \"Protocols/%@-Protocol.h\"\n",p.name];
            }
        }
        if (muProtocolsImports.length){
            [contents appendString:@"\n"];
            [contents appendString:@"// - Protocols -\n"];
            [contents appendString: muProtocolsImports];
        }
    }

    // protocols referenced in methods and properties but imports are not required.
    NSMutableString * muProtocolDeclarations = [NSMutableString new];
 
    [self.referencedProtocolNames enumerateObjectsUsingBlock:^(NSString * referencedProtocol, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([self.context validateProtocolName:referencedProtocol inModule:self.frameworkName]){
            if (![adoptedProtocolNames containsObject:referencedProtocol]){
                [adoptedProtocolNames addObject:referencedProtocol];
                [muProtocolDeclarations appendFormat:@"@protocol %@;\n",referencedProtocol];
            }
        }
    }];
    if (muProtocolDeclarations.length){
        [contents appendString:@"\n"];
        [contents appendString:@"// - Forward Protocols -\n"];
        [contents appendString: muProtocolDeclarations];
    }
    
    [contents appendString:@"\n"];
    [contents appendFormat:@"@interface %@ : %@",self.name,self.superclassName];
    
//    [contents appendFormat:@"@end\n\n"];
//    [contents appendFormat:@"@interface %@ (Methods)",self.name];
    // add adopted protocols
    if (self.adoptedProtocols.count){
        [contents appendFormat:@" //<%@>",[[self.adoptedProtocols valueForKey:@"name"] componentsJoinedByString:@","]];
    }
    
    [contents appendString:@"\n{\n"];
    for (CDIVar * ivar in self.ivarDecls){
        [contents appendFormat:@"      // %@ %@ // %ld\n",ivar.type.declaration?:@"??", ivar.name, ivar.offset];
    }
    [contents appendString:@"}"];
   
    [contents appendString:@"\n\n"];

    NSMutableString * muProperties = [NSMutableString new];
    NSMutableArray * getterSetterNames = [NSMutableArray new];
    if ([self.name isEqualToString:@"MCMessage"]){
        
    }
    
    NSArray * sortedProps = [self.properties sortedArrayUsingComparator:^NSComparisonResult(CDProperty * obj1, CDProperty * obj2) {
        return [obj1.name caseInsensitiveCompare:obj2.name];
    }];
    NSArray * sortedMethods = [self.methods sortedArrayUsingComparator:^NSComparisonResult(CDMethod * obj1, CDMethod * obj2) {
        return [obj1.selector caseInsensitiveCompare:obj2.selector];
    }];
    
    [sortedProps enumerateObjectsUsingBlock:^(CDProperty * property, NSUInteger idx, BOOL * _Nonnull stop) {
        [muProperties appendFormat:@"%@\n",property.declaration];
        NSString * scope =[property.attributes containsObject:@"class"]?@"+":@"-";
        
        if (property.getterName){
            [getterSetterNames addObject:[scope stringByAppendingString:property.getterName]];
        }
        if (property.setterName){
            [getterSetterNames addObject:[scope stringByAppendingString:property.setterName]];
        }
    }];
    if (muProperties.length){
        [contents appendString: muProperties];
        [contents appendString:@"\n"];
    }
    
    
    NSMutableString * muMethods = [NSMutableString new];
    
    [sortedMethods enumerateObjectsUsingBlock:^(CDMethod * method, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![getterSetterNames containsObject: method.selector]){
            [muMethods appendFormat:@"%@\n",method.declaration];
        }
    }];
    
    if (muMethods.length){
        [contents appendString:muMethods];
        [contents appendString:@"\n"];
    }

    [contents appendString: @"@end\n"];
    [contents appendString:@"\n"];
    [contents appendString:@"#pragma clang diagnostic pop\n"];
  
    return [contents copy];
}


-(NSArray <CDClass*> *)ancestry{
    NSMutableArray * muAncestry = [NSMutableArray new];
    if (self.superCDClass){
        [muAncestry addObjectsFromArray:self.superCDClass.ancestry];
        [muAncestry addObject:self.superCDClass];
    }
    return [muAncestry copy];
}
@end
