//
//  NSData+SHA1.h
//  dsdump
//
//  Created by Scott Morrison on 2020-07-27.
//  Copyright © 2020 Selander. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSData (Digest)
- (NSString *)sha1Digest;
@end

NS_ASSUME_NONNULL_END
