//
//  CDVariable.m
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-09.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import "CDParameter.h"
#import "CDType.h"
#import "NSScanner+methodLineScanner.h"
#import "CDContext.h"
@interface CDParameter ()
@property (weak) CDContext * context;
@end
@implementation CDParameter

+(instancetype)cdParameterWithDeclaration:(NSString*)declaration{
    NSScanner * scanner = [NSScanner scannerWithString:declaration];
    
    [scanner setCharactersToBeSkipped:nil];
    CDType * obj;
    NSString * name;
    if ([scanner scanUpToCharacters:@",;)" intoCDType:&obj name:&name]){
        CDParameter * param = [CDParameter new];
        param.type = obj;
        param.name = name;
        return param;
    }
    
    return nil;
}
-(NSString*)description{
    return [[super description] stringByAppendingFormat:@"(%@ %@)",self.name,self.type.description];
}
-(NSString*)methodParameterDeclaration{
    return [NSString stringWithFormat:@"(%@)%@",self.type.declaration,self.name];
}
-(NSString*)blockParameterDeclaration{
    NSString * name =(self.name && ![self.name isEqualToString:@"void"])?[NSString stringWithFormat:@" %@",self.name]:@"";
    return [NSString stringWithFormat:@"%@%@",self.type.declaration,name];
}

-(NSArray <NSString*> * ) referencedClassNames{
    NSMutableSet * muClasses = [NSMutableSet new];
    [muClasses addObjectsFromArray:[self.type referencedClassNames]];
    return [muClasses allObjects];
}
-(NSArray <NSString*> * ) referencedProtocolNames{
    NSMutableSet * muProtocols = [NSMutableSet new];
    [muProtocols addObjectsFromArray:[self.type referencedProtocolNames]];
    return [muProtocols allObjects];
}
@end
