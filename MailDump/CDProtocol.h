//
//  CDProtocol.h
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-10.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CDFileObject-Protocol.h"

NS_ASSUME_NONNULL_BEGIN
@class CDMethod, CDProperty, CDContext;

@interface CDProtocol : NSObject <CDFileObject>

@property (copy) NSString * name;
@property (weak) id <CDFileObjectDelegate> delegate;
@property (strong) NSString * moduleName;

@property (strong) NSArray <NSString*> * protocolNames;
@property (strong) NSArray <CDProtocol*> * adoptedProtocols;
@property (readonly) NSArray <CDProtocol*> * ancestry;

@property (strong) NSArray <CDMethod*> * requiredMethods;
@property (strong) NSArray <CDMethod*> * optionalMethods;

@property (strong) NSArray <CDProperty*> * properties;

@property (strong) NSString * OSVersion;
@property (strong) NSString * OSBuild;
@property (strong) NSString * dateString;

@property (readonly) NSArray <NSString*> * referencedProtocolNames;
@property (readonly) NSArray <NSString*> * referencedClassNames;
@property (readonly) NSString * headerFileContents;


+(instancetype)withObjcProtocol:(Protocol*)objcProtocol
                moduleName:(NSString*)moduleName
                        context:(CDContext*)context;

-(NSSet*)adoptProtocolsInMap:(NSDictionary*)protocolMap;

-(void)importHeaderFileAtPath:(NSString*)path;

@end

NS_ASSUME_NONNULL_END
