//
//  CDProtocol.m
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-10.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import "CDProtocol.h"
#import "CDMethod.h"
#import "CDParameter.h"
#import "CDProperty.h"
#import "CDType.h"
#import "CDContext.h"


#import "NSScanner+methodLineScanner.h"


@interface CDProtocol ()
@property (weak) CDContext * context;
@property (copy) NSString * _name_;

@end

@implementation CDProtocol

+(instancetype)withObjcProtocol:(Protocol*)objcProtocol moduleName:(NSString*)moduleName context:(CDContext*)context {

    const char* _protoName = protocol_getName(objcProtocol);
    NSString * protoName =[[NSString stringWithUTF8String:_protoName] stringByReplacingOccurrencesOfString:@"." withString:@"_"];
    @synchronized (context) {
        if (context.protocols[protoName]){
            return context.protocols[protoName];
        }
    }
    if ([context validateProtocolName:protoName inModule:moduleName] || [protoName isEqualToString:@"NSObject"]){
        CDProtocol * cdProtocol = [[CDProtocol alloc] initWithObjcProtocol:objcProtocol moduleName:moduleName context:context];
        @synchronized (context) {
            if (!context.protocols) context.protocols = [NSMutableDictionary new];
            context.protocols[protoName] = cdProtocol;
        }
        return cdProtocol;
    }
    return nil;
}

-(instancetype)initWithObjcProtocol:(Protocol*)objcProtocol moduleName:(NSString*)moduleName context:(CDContext*)context {
    self = [self init];
    
    const char* _protoName = protocol_getName(objcProtocol);
    NSString * protoName =[[NSString stringWithUTF8String:_protoName] stringByReplacingOccurrencesOfString:@"." withString:@"_"];
 
    self.context = context;
    self.moduleName = moduleName;
    self.name = protoName;
    self.adoptedProtocols = @[];

    { // protocols
        NSMutableArray * adoptedProtocols = [NSMutableArray new];
        unsigned int protoCount = 0;
        Protocol * __unsafe_unretained * protocols = protocol_copyProtocolList(objcProtocol, &protoCount);
        while (protoCount--){
            Protocol * p  = protocols[protoCount];
            CDProtocol * adoptedProtocol = [CDProtocol withObjcProtocol:p moduleName:moduleName context:context];
            if (adoptedProtocol){
                [adoptedProtocols addObject:adoptedProtocol];
            }
        }
        if (protocols) free(protocols);
        self.adoptedProtocols = adoptedProtocols;
    }
    
    { // properties
        unsigned int propertyCount = 0;
        NSMutableArray * cdProperties = [NSMutableArray new];
        objc_property_t * objcProperties = protocol_copyPropertyList(objcProtocol, &propertyCount);
        while (propertyCount--){
            objc_property_t p = objcProperties[propertyCount];
            CDProperty * prop = [CDProperty withObjcProperty:p context:context];
            if ([context validatePropertyName:prop.name]){
                [cdProperties addObject:prop];
                //[context recordStructs:prop.referencedStructs];
            }
        }
        if (objcProperties) free (objcProperties);
        self.properties = cdProperties;
    }
    NSMutableArray* cdMethods = [NSMutableArray new];
    
    { // required classMethods
        unsigned int methodCount = 0;
        struct objc_method_description * mds = protocol_copyMethodDescriptionList(objcProtocol, YES, NO, &methodCount);
        while (methodCount--){
            struct objc_method_description md = mds[methodCount];
            CDMethod * method = [CDMethod withObjcMethodDescription:md context: context isInstanceMethod:NO ];
            if ([context validateMethodName:method.selector]){
                [cdMethods addObject:method];
            }
        }
        if (mds) free (mds);
    }
    { // required instance
        unsigned int methodCount = 0;
        struct objc_method_description * mds = protocol_copyMethodDescriptionList(objcProtocol, YES, YES, &methodCount);
        while (methodCount--){
            struct objc_method_description md = mds[methodCount];
            CDMethod * method = [CDMethod withObjcMethodDescription:md context: context isInstanceMethod:YES];
            if ([context validateMethodName:method.selector]){
                [cdMethods addObject:method];
            }
        }
        if (mds) free (mds);
    }
    
    
    self.requiredMethods = cdMethods;
    cdMethods = [NSMutableArray new];

    { // optional classMethods
        unsigned int methodCount = 0;
        struct objc_method_description * mds = protocol_copyMethodDescriptionList(objcProtocol,  NO, NO, &methodCount);
        while (methodCount--){
            struct objc_method_description md = mds[methodCount];
            CDMethod * method = [CDMethod withObjcMethodDescription:md context: context isInstanceMethod:NO];
            if ([context validateMethodName:method.selector]){
                [cdMethods addObject:method];
            }
        }
        if (mds) free (mds);
    }
    { // optional instance
        unsigned int methodCount = 0;
        struct objc_method_description * mds = protocol_copyMethodDescriptionList(objcProtocol, NO, YES, &methodCount);
        while (methodCount--){
            struct objc_method_description md = mds[methodCount];
            CDMethod * method = [CDMethod withObjcMethodDescription:md context: context isInstanceMethod:YES];
            if ([context validateMethodName:method.selector]){
                [cdMethods addObject:method];
            }
        }
        if (mds) free (mds);
    }
    

    
    self.optionalMethods = cdMethods;
    
    
    return self;
}

-(void)setName:(NSString *)name{
    self._name_ =  [name stringByReplacingOccurrencesOfString:@"." withString:@"_"];
}
-(NSString *)name{
    return self._name_;
}

-(void)importHeaderFileAtPath:(NSString*)path{
    NSError * fileReadError= nil;
    NSString * fileContents = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&fileReadError] ;
    NSMutableArray * properties = [NSMutableArray new];
    NSMutableArray * methods = [NSMutableArray new];
    NSMutableArray * ivars = [NSMutableArray new];
    NSMutableArray * uncollectedLines = [NSMutableArray new];
    NSMutableArray * imports = [NSMutableArray new];
    NSMutableArray * protocolDeclarations = [NSMutableArray new];
    NSMutableArray * classDeclarations = [NSMutableArray new];
    
    __block BOOL collectingIvars = NO;
    __block BOOL buildingClass = NO;
    [fileContents enumerateLinesUsingBlock:^(NSString *aLine, BOOL *stop) {
        if ([aLine hasPrefix:@"@protocol"]){
            [self scanProtocolDeclarationLine:aLine];
            buildingClass=YES;
        }
        if ([aLine hasPrefix:@"@end"]){
            buildingClass = NO;
            self.properties = properties;
            self.methods = methods;
        }
        else if (buildingClass){
            if ([aLine hasPrefix:@"@property"]){
                CDProperty * property = [CDProperty propertyWithLine:aLine];
                [properties addObject:property];
            }
            else if ([aLine hasPrefix:@"-"]){
                CDMethod * method = [CDMethod methodWithLine:aLine];
                [methods addObject:method];
            }
            else if ([aLine hasPrefix:@"+"]){
                CDMethod * method = [CDMethod methodWithLine:aLine];
                [methods addObject:method];
            }
            else if ([aLine hasPrefix:@"{"]){
                collectingIvars = YES;
            }
            else if ([aLine hasPrefix:@"}"]){
                collectingIvars = NO;
            }
            else if (collectingIvars){
                CDType * ivar = [CDType typeWithIvarLine:aLine];
                if (ivar){
                    [ivars addObject:ivar];
                }
            }
        }
        else if ([aLine hasPrefix:@"#import"]){
            [imports addObject:aLine];
        }
        else if ([aLine hasPrefix:@"@class"]){
            [classDeclarations addObject:aLine];
        }
        else if ([aLine hasPrefix:@"@protocol"] && [aLine hasSuffix:@";"]){
            [protocolDeclarations addObject:aLine];
        }
        else if (aLine.length>0){
            [uncollectedLines addObject:aLine];
        }
    }];
}

-(NSString*) pathInProject{
    return [NSString stringWithFormat:@"Protocols/%@-Protocol.h",self.name];
}
-(void)scanProtocolDeclarationLine:(NSString*)line{
    //@interface MFLibrary : NSObject <EDSearchableIndexReasonProvider, MCActivityTarget>
    //@interface ABAddressBookPersion (MailAdditions) <EDSearchableIndexReasonProvider, MCActivityTarget>
    
    NSScanner * scanner = [NSScanner scannerWithString:[line trimmedString]];
    [scanner setCharactersToBeSkipped:nil];
    if ([scanner scanString:@"@protocol" intoString:nil]){
        [scanner scanWhiteSpace];
        NSString * className;
        if ([scanner scanUpToString:@"<" intoString:&className]){
            [scanner advance:1];
             self.name = [className trimmedString];
            NSString * protocolList;
            if ([scanner scanUpToString:@">" intoString:&protocolList]){
                [scanner advance:1];
                NSArray * rawProtocols = [protocolList componentsSeparatedByString:@","];
                NSMutableArray * muProtocols = [NSMutableArray new];
                for (NSString * rawProtocol in rawProtocols){
                    NSString * protocol = [rawProtocol trimmedString];
                    if (protocol.length){
                        [muProtocols addObject:protocol];
                    }
                }
                self.protocolNames = [muProtocols copy];;
            }
        }
        
    }
    
}
-(NSArray <NSString*> * )referencedProtocolNames{
    NSMutableSet * muProtocols = [NSMutableSet set];
    [muProtocols addObjectsFromArray:self.protocolNames];
    
    for (CDMethod * method in self.requiredMethods){
        [muProtocols addObjectsFromArray:method.referencedProtocolNames];
    }
    for (CDMethod * method in self.optionalMethods){
        [muProtocols addObjectsFromArray:method.referencedProtocolNames];
    }
    for (CDProperty * prop in self.properties){
        [muProtocols addObjectsFromArray:prop.referencedProtocolNames];
    }
    return [muProtocols allObjects];
}
-(NSArray <NSString*> * ) referencedClassNames{
    NSMutableSet * muClasses = [NSMutableSet set];
    for (CDProperty * property in self.properties){
        [muClasses addObjectsFromArray:property.referencedClassNames];
    }
    for (CDMethod * method in self.requiredMethods){
        [muClasses addObjectsFromArray:method.referencedClassNames];
    }
    for (CDMethod * method in self.optionalMethods){
        [muClasses addObjectsFromArray:method.referencedClassNames];
    }
    return [muClasses allObjects];
}


-(NSSet*)adoptProtocolsInMap:(NSDictionary*)protocolMap{
    NSMutableSet * adoptedProtocols = [NSMutableSet new];
    if (self.adoptedProtocols){
        [adoptedProtocols addObjectsFromArray:self.adoptedProtocols];
        [adoptedProtocols addObject:self];
        return adoptedProtocols;
    }
    for (NSString * protocolName in self.protocolNames){
        CDProtocol* aProtocol = protocolMap[protocolName];
        if (aProtocol){
            [adoptedProtocols unionSet:[aProtocol adoptProtocolsInMap:protocolMap]];
        }
    }
    self.adoptedProtocols = [adoptedProtocols allObjects];
    [adoptedProtocols addObject:self];
    return adoptedProtocols;
}

-(NSString*)description{
    return [[super description] stringByAppendingFormat:@"@protocol %@ ",self.name];
}


-(NSString*)headerFileContents{
    NSMutableString * contents = [NSMutableString new];
    [contents appendString:@"//\n"];
    [contents appendFormat:@"//   %@-Protocol.h\n",self.name];
    [contents appendFormat:@"//\n"];
    [contents appendFormat:@"//   OS Version: %@\n",self.context.osVersion];
    [contents appendFormat:@"//\n"];
    [contents appendFormat:@"//   Mail Version: %@\n",self.context.mailVersion];
    [contents appendFormat:@"//   Mail UUID: %@\n",self.context.compatibilityUUID];
   
    [contents appendString:@"//\n\n"];

    [contents appendString:@"#import \"MailCommon.h\"\n\n"];
    
    [contents appendString:@"\n"];
    [contents appendString:@"#pragma clang diagnostic push\n"];
    [contents appendString:@"#pragma clang diagnostic ignored \"-Wdeprecated-declarations\"\n"];
    [contents appendString:@"\n\n"];
    [contents appendFormat:@"#ifndef OMIT_PROTOCOL_%@\n\n",self.name];
    

    if ([self.name isEqualToString:@"ECMessage"]){
        
    }
    NSMutableSet * adoptedProtocolNames = [NSMutableSet new];
    NSArray * ancestry = self.ancestry;
    
    if (ancestry.count){
        NSMutableString * muProtocolsImports = [NSMutableString new];
        for (CDProtocol * p in ancestry){
            if ([self.context validateProtocolName:p.name inModule:self.moduleName]){
                [adoptedProtocolNames addObject:p.name];
                [muProtocolsImports appendFormat:@"#import \"Protocols/%@-Protocol.h\"\n",p.name];
            }
        }
        if (muProtocolsImports.length){
            [contents appendString:@"// - Protocols -\n"];
            [contents appendString: muProtocolsImports];
            [contents appendString:@"\n"];
        }
    }
  
    NSMutableString * muClassDeclarations = [NSMutableString new];
    [self.referencedClassNames enumerateObjectsUsingBlock:^(NSString * referencedClass, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([self.context validateClassName:referencedClass inModule:self.moduleName]){
            [muClassDeclarations appendFormat:@"@class %@;\n",referencedClass];
        }
    }];
    
    if (muClassDeclarations.length){
        if (muClassDeclarations.length){
            [contents appendString:@"// - Classes -\n"];
            [contents appendString: muClassDeclarations];
            [contents appendString:@"\n"];
        }
    }

    // protocols referenced in methods and properties but imports are not required.
    NSMutableString * muProtocolDeclarations = [NSMutableString new];
 
    [self.referencedProtocolNames enumerateObjectsUsingBlock:^(NSString * referencedProtocol, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([self.context validateProtocolName:referencedProtocol inModule:self.moduleName]){
            if (![adoptedProtocolNames containsObject:referencedProtocol]){
                [adoptedProtocolNames addObject:referencedProtocol];
                [muProtocolDeclarations appendFormat:@"@protocol %@;\n",referencedProtocol];
            }
        }
    }];
    if (muProtocolDeclarations.length){
        [contents appendString:@"// - Protocols -\n"];
        [contents appendString: muProtocolDeclarations];
        [contents appendString:@"\n"];
    }
   
    [contents appendFormat:@"@protocol %@ ", self.name];
    // add adopted protocols
    NSMutableArray * protocolNames =[[self.adoptedProtocols valueForKey:@"name"] mutableCopy];
    if ([protocolNames containsObject:@"NSObject"]){
        [contents appendFormat:@" <NSObject>"];
        [protocolNames removeObject:@"NSObject"];
    }
    if (protocolNames.count){
        [contents appendFormat:@" //<%@>",[protocolNames componentsJoinedByString:@","]];
    }
    
    [contents appendString:@"\n\n"];
    
    NSMutableString * muProperties = [NSMutableString new];
    NSMutableArray * getterSetterNames = [NSMutableArray new];
    NSMutableSet <NSString *> * declarations = [NSMutableSet new];
  
    [self.properties enumerateObjectsUsingBlock:^(CDProperty * property, NSUInteger idx, BOOL * _Nonnull stop) {
        NSString * decl =  property.declaration;
       if (![declarations containsObject:decl]){
            [muProperties appendFormat:@"%@\n",decl];
            [declarations addObject:decl];
        }
        NSString * scope =[property.attributes containsObject:@"class"]?@"+":@"-";
        
        if (property.getterName){
            [getterSetterNames addObject:[scope stringByAppendingString:property.getterName]];
        }
        if (property.setterName){
            [getterSetterNames addObject:[scope stringByAppendingString:property.setterName]];
        }
    }];
    if (muProperties.length){
        [contents appendString: muProperties];
        [contents appendString:@"\n"];
    }
    NSMutableString * muReqMethods = [NSMutableString new];
     
    [self.requiredMethods enumerateObjectsUsingBlock:^(CDMethod * method, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![getterSetterNames containsObject: method.selector]){
            NSString * decl = method.declaration;
            if (![declarations containsObject:decl]){
                [muReqMethods appendFormat:@"%@\n",decl];
                [declarations addObject:decl];
            }
        }
    }];
    
    if (muReqMethods.length){
        [contents appendString:@"@required\n"];
        [contents appendString:muReqMethods];
        [contents appendString:@"\n"];
  }
    
    NSMutableString * muOptMethods = [NSMutableString new];
    [self.optionalMethods enumerateObjectsUsingBlock:^(CDMethod * method, NSUInteger idx, BOOL * _Nonnull stop) {
        if (![getterSetterNames containsObject: method.selector]){
            NSString * decl = method.declaration;
            if (![declarations containsObject:decl]){
                [muOptMethods appendFormat:@"%@\n",decl];
                [declarations addObject:decl];
            }
        }
    }];
    if (muOptMethods.length){
        [contents appendString:@"@optional\n"];
        [contents appendString:muOptMethods];
        [contents appendString:@"\n"];
}
    
    [contents appendString: @"@end\n"];
    [contents appendString:@"\n"];
    [contents appendFormat:@"#endif //ifndef OMIT_PROTOCOL_%@\n\n",self.name];
    
    [contents appendString:@"#pragma clang diagnostic pop\n"];
 
    return [contents copy];


}


-(NSArray <CDProtocol*> *)ancestry{
    NSMutableArray * muAncestry = [NSMutableArray new];
    for (CDProtocol * parent in self.adoptedProtocols){
        [muAncestry addObjectsFromArray:parent.ancestry];
        [muAncestry addObject:parent];
    }
    return [muAncestry copy];
}
@end
