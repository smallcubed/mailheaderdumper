//
//  NSScanner+methodLineScanner.m
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-09.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import "NSScanner+methodLineScanner.h"
#import "CDType.h"
#import "CDParameter.h"
#import "NSData+Digest.h"
#import <AppKit/AppKit.h>


typedef NS_ENUM(char,ObjcTypeChar){
    _C_ID       ='@',
    _C_CLASS    ='#',
    _C_SEL      =':',
    _C_CHR      ='c',
    _C_UCHR     ='C',
    _C_SHT      ='s',
    _C_USHT     ='S',
    _C_INT      ='i',
    _C_UINT     ='I',
    _C_LNG      ='l',
    _C_ULNG     ='L',
    _C_LNG_LNG  ='q',
    _C_ULNG_LNG ='Q',
    _C_FLT      ='f',
    _C_DBL      ='d',
    _C_BFLD     ='b',
    _C_BOOL     ='B',
    _C_VOID     ='v',
    _C_UNDEF    ='?',
    _C_PTR      ='^',
    _C_CHARPTR  ='*',
    _C_ATOM     ='%',
    _C_ARY_B    ='[',
    _C_ARY_E    =']',
    _C_UNION_B  ='(',
    _C_UNION_E  =')',
    _C_STRUCT_B ='{',
    _C_STRUCT_E ='}',
    _C_VECTOR   ='!',
    _C_CONST    ='r',
    _C_ONEWAY   ='V',
    _C_OUT      ='o',
    _C_ATOMIC   ='A',
    
};


@implementation NSString (trimming)
-(NSString*)trimmedString{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}
@end

@implementation NSScanner (methodLineScanner)
-(BOOL)scanToClosingParathensisIntoString:(NSString**)aString{
    NSUInteger startLocation = self.scanLocation;
    if ([self currentCharacter]!='('){
        return NO;
    }
    if (![self advance:1]){
        self.scanLocation = startLocation;
        return NO;
    }
    NSInteger pDepth = 1;
    NSInteger lastLocation = -1;
    while(![self isAtEnd]){
        if ((NSInteger)self.scanLocation <= (NSInteger)lastLocation){
            // clear the reference parameters if they have been set
            self.scanLocation = startLocation;
            return NO;
        }
        lastLocation = (NSInteger)self.scanLocation;
        if (self.currentCharacter == ')'){
            pDepth--;
            if (pDepth ==0){
                if (aString){
                   *aString =  [self.string substringWithRange:NSMakeRange(startLocation,lastLocation-startLocation+1)];
                }
                [self advance:1];
                return YES;
            }
        }
        else if (self.currentCharacter == '('){
            pDepth++;
        }
        if (![self advance:1]){
            self.scanLocation = startLocation;
            return NO;
        }
    }
    return NO;
}


-(void(^)( BOOL(^result)(void) ))blockWithCompletion:(void(^)( NSInteger(^)(NSString* p1, void(^p2)(void)) ))completion{
    return ^(BOOL(^b)(void)){b();};
}
//(void(^)( NSInteger(^)(id <protocol>, void(^)(void))
// (_scalar_(^)(_blockParams_))
// _scalar_(^varName)(_blockParams_);
// _scalar_ varName;
// (_scalar_)
// (pointer *)

//(unsigned long long (^)(_blockParams))

// (id <procotolName> (^)(_blockParams_))
// (NS(Mutable)?Array <(id|type*)> *
// (NS(Mutable)?Set <(id|type*)> *
// (NS(Mutable)?Dictionary <(id|type*),(id|type*)> *

-(BOOL)scanUpToCharacters:(NSString*)stopChars intoCDType:(CDType**) byRefType name:(NSString**)name{

    NSUInteger startLocation = self.scanLocation;
    if (byRefType) *byRefType = nil;
    if (name) *name = nil;
    
    NSCharacterSet * whiteSpace = [NSCharacterSet whitespaceCharacterSet];
    [self scanWhiteSpace];
    if (self.currentCharacter=='('){
        [self advance:1];
        if ([self scanUpToCharacters:@")" intoCDType:byRefType name:nil]){
            [self advance:1];
            [self scanWhiteSpace];
            [self scanUpToCharactersFromString:stopChars intoString:name];
            return YES;
        }
        self.scanLocation = startLocation;
        return NO;
    }
    
    NSCharacterSet * clientStopChars = [NSCharacterSet characterSetWithCharactersInString:stopChars];
    NSMutableCharacterSet * muStopChars = [NSMutableCharacterSet characterSetWithCharactersInString:@"(<, "];
    [muStopChars formUnionWithCharacterSet:clientStopChars];
    
    NSInteger lastLocation = -1;
    NSMutableArray * words = [NSMutableArray new];
    while(![self isAtEnd]){
        if ((NSInteger)self.scanLocation <= (NSInteger)lastLocation){
            self.scanLocation = startLocation;
            return NO;
        }
        lastLocation = (NSInteger)self.scanLocation;
        NSString * word;
        
        [self scanUpToCharactersFromSet:muStopChars
                             intoString:&word];
        if (word){
            [words addObject:word];
        }
        if (self.currentCharacter == ' '){
            [self scanWhiteSpace];
        }
        else if (self.currentCharacter == '('){
            // start of a block
            CDType * outType;
            if (byRefType) {
                outType = *byRefType?:[CDType new];
                *byRefType = outType;
            }
            outType.typeName =[words componentsJoinedByString:@" "];
         
            [self advance:1];
            [self scanWhiteSpace];
            [self scanString:@"^" intoString:nil];
            [self scanWhiteSpace];
            NSString * blockName;
            [self scanUpToString:@")" intoString:&blockName];
            if (name) *name = [blockName stringByTrimmingCharactersInSet: whiteSpace];
            [self advance:1];
            [self scanWhiteSpace];
            if (self.currentCharacter == '('){
                //block parameterList;
                [self advance:1];
                CDType * paramType;
                NSString * paramName;
                NSMutableArray * params = [NSMutableArray new];
                // recursively scan the block parameter list
                while ([self scanUpToCharacters:@",)" intoCDType:&paramType name:&paramName]){
                    CDParameter *param = [CDParameter new];
                    param.name = paramName;
                    param.type = paramType;
                    [params addObject:param];
                    if (self.currentCharacter==',') [self advance:1];
                }
                outType.blockParameters = params;
                [self advance:1];
            }
        }
        else if (self.currentCharacter == '<'){
            [self advance:1];
            NSString * protocolList;
            [self scanUpToString:@">" intoString:&protocolList];
            [self advance:1];
            CDType * outType;
            if (byRefType) {
                outType = *byRefType?:[CDType new];
                *byRefType = outType;
            }
            outType.typeName =[words componentsJoinedByString:@" "];
            outType.protocolNames =[[protocolList componentsSeparatedByString:@","] valueForKey:@"trimmedString"];
        }
        else if ([clientStopChars characterIsMember:self.currentCharacter] || self.isAtEnd){
            NSMutableArray * typeParts = [NSMutableArray new];
            CDType * outType;
            if (byRefType) {
                outType = *byRefType?:[CDType new];
                *byRefType = outType;
            }
            NSString * lastWord = [words lastObject];
            
            if ([@[@"struct",@"unsigned",@"short"] containsObject: [words firstObject]]){
                [typeParts addObject:[words firstObject]];
                [words removeObjectAtIndex:0];
            }
            
            if (words.count>0){
                if ( words.count <=1){
                    [typeParts addObject:[words firstObject]];
                    lastWord = nil;
                }
                else{
                    NSArray * typeWords = [words subarrayWithRange:NSMakeRange(0,words.count-1)];
                    lastWord = [words lastObject];
                    [typeWords enumerateObjectsUsingBlock:^(NSString * _Nonnull typeWord, NSUInteger idx, BOOL * _Nonnull stop) {
                        if ([typeWord isEqualToString:@"*"]){
                            NSString * pointerType = [[typeParts lastObject] stringByAppendingString:@"*"];
                            [typeParts removeLastObject];
                            [typeParts addObject:pointerType];
                        }
                        else{
                            [typeParts addObject:typeWord];
                        }
                    }];
                    if ([@[@"int",@"long",@"char",@"short",@"float",@"double"] containsObject:lastWord]){
                        [typeParts addObject:lastWord];
                        lastWord = nil;
                    }
                    else if ([lastWord hasPrefix:@"*"]){
                        NSString * pointerType = [[typeParts lastObject] stringByAppendingString:@"*"];
                        [typeParts removeLastObject];
                        [typeParts addObject:pointerType];
                        lastWord=[lastWord substringFromIndex:1];
                    }
                }
            }
            outType.typeName = [typeParts componentsJoinedByString:@" "];
            if (name) {
                if (!*name){
                    *name = lastWord.length?lastWord:nil;
                }
            }
            
            break;
        }
    }
    
    return self.scanLocation>startLocation;
}

-(NSString*)remainder{
    return [self.string substringFromIndex:self.scanLocation];
}
-(unichar)currentCharacter{
    if ([self isAtEnd]) return 0;
    return [[self string] characterAtIndex:[self scanLocation]];
}
-(unichar)characterAtOffset:(NSInteger)offset{
    if (self.scanLocation+offset>=[self.string length]) return 0;
    if ((NSInteger)(self.scanLocation+offset)<0) return 0 ;
    return [self.string characterAtIndex:self.scanLocation+offset];
}

-(NSUInteger)advance:(NSUInteger)characterCount{
    // advanced specified characters -- returns the number of characters actually advanced.
    if ([self isAtEnd]) return 0;
    NSUInteger startLocation = [self scanLocation];
    NSUInteger newlocation = MIN(startLocation+characterCount,[[self string]length]);
    [self setScanLocation:newlocation];
    return (newlocation - startLocation);
}
-(NSUInteger)rewind:(NSUInteger)characterCount{
    // rewinds specified characters -- returns the number of characters actually rewound.
    if ([self scanLocation]==0) return 0;
    NSUInteger startLocation = [self scanLocation];
    NSUInteger newlocation = MAX(startLocation-characterCount,0);
    [self setScanLocation:newlocation];
    return (startLocation-newlocation);
}

-(BOOL)scanWhiteSpace{
    NSUInteger startLocation = self.scanLocation;
    [self scanCharactersFromSet:[NSCharacterSet whitespaceCharacterSet] intoString:nil];
    return self.scanLocation>startLocation;
};
        
-(BOOL)scanUpToCharactersFromString:(NSString*)charString intoString:(NSString* _Nonnull *_Nullable)aString{
    return [self scanUpToCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:charString] intoString:aString];
}
-(BOOL)scanCharactersFromString:(NSString*)charString intoString:(NSString* _Nonnull *_Nullable)aString{
    return [self scanUpToCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:charString] intoString:aString];
}

-(BOOL)scanObjcTypeEncoding:(NSString**)outEncoding{
   NSUInteger startLocation = self.scanLocation;
    ObjcTypeChar curChar = self.currentCharacter;
    if (curChar){
        [self advance:1];
        switch (curChar) {
            case _C_ID:
                if (self.currentCharacter == '"'){
                    [self advance:1];
                    [self scanUpToString:@"\"" intoString:nil];
                    [self advance:1];
                }
                else if (self.currentCharacter == '?'){
                    [self advance:1];
                }
                break;
            case _C_BFLD:
                [self scanInteger:nil];
                break;
            case _C_UNDEF:
                break;
            case _C_PTR:
                [self scanObjcTypeEncoding:nil];
                break;
            case _C_STRUCT_B:{
                // 2 forms {?==XXxx} and {Name=}
                NSInteger position =self.scanLocation;
                [self scanUpToCharactersFromString:@"=}" intoString:nil];
                if (self.currentCharacter=='='){
                    [self advance:1];
                    while (self.currentCharacter != '}'){
                        [self scanObjcTypeEncoding:nil];
                    }
                    [self advance:1];
                }
                else{
                    [self setScanLocation:position];
                    if ([self scanUpToString:@"}" intoString:nil]){
                        [self advance:1];
                    }
                }
                break;
            }
            case _C_UNION_B:{
                while ([self scanObjcTypeEncoding:nil] && ![self scanString:@")" intoString:nil]){
                }
                break;
            }
            case _C_CONST:
            case _C_VECTOR :
            case _C_OUT :
            case _C_ONEWAY :
            case _C_ATOMIC:
                [self scanObjcTypeEncoding:nil];
                break;
            case _C_ARY_B:
                [self scanInteger:nil];
                [self scanObjcTypeEncoding:nil];
                [self advance:1];
                break;
    
            case _C_ATOM:
            
            default:
                break;
        }
    }
    if ( self.scanLocation>startLocation){
        if (outEncoding){
            *outEncoding = [self.string substringWithRange:NSMakeRange(startLocation,self.scanLocation-startLocation)];
        }
        return YES;
    }
    return NO;
}

-(BOOL)scanObjcTypeEncodingIntoCDType:(CDType *_Nonnull *_Nullable)cdType{
     NSUInteger startLocation = self.scanLocation;
    ObjcTypeChar curChar = self.currentCharacter;
    [self advance:1];
    CDType * scannedType = nil;
    scannedType = [CDType withPrimitiveObjcEncoding:curChar];
    
    switch (curChar) {
        case _C_ID:
            if (self.currentCharacter == '"'){
                [self advance:1];
                NSString * namedObjectType = nil;
                
                [self scanUpToString:@"\"" intoString:&namedObjectType];
                scannedType.objcEncoding = [NSString stringWithFormat:@"@\"%@\"",namedObjectType];
                [self advance:1];
                NSRange protocolStart=[namedObjectType rangeOfString:@"<"];
                NSString * protocolList = nil;
                if (protocolStart.length){
                    protocolList = [namedObjectType substringFromIndex:protocolStart.location];
                    protocolList = [protocolList stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
                    scannedType.protocolNames = [protocolList componentsSeparatedByString:@","];
                    namedObjectType = [namedObjectType substringToIndex:protocolStart.location];
                }
                if (namedObjectType.length>0){
                    if (scannedType.protocolNames){
                        namedObjectType = [namedObjectType stringByAppendingFormat:@"<%@>",[scannedType.protocolNames componentsJoinedByString:@","] ];
                    }
                     scannedType.pointer = 1;
                }
                else{
                    namedObjectType = @"id ";
                    if (scannedType.protocolNames){
                        namedObjectType = [namedObjectType stringByAppendingFormat:@"<%@>",[scannedType.protocolNames componentsJoinedByString:@","] ];
                    }
                }
                scannedType.typeName = namedObjectType;
            }
            else if (self.currentCharacter == '?'){
                [self advance:1];
                scannedType.typeName=@"CDUnknownBlockType";
                scannedType.objcEncoding = @"@?";
            }

            break;
        case _C_BFLD:{
            NSInteger fieldSize;
            [self scanInteger:&fieldSize];
            scannedType.objcEncoding = [NSString stringWithFormat:@"B%ld",fieldSize];
            scannedType.typeName = [NSString stringWithFormat:@"NSInteger:%ld",fieldSize];
            break;
        }
        case _C_UNDEF:
            scannedType.typeName = @"?";
            break;
        case _C_PTR:{
            CDType * subType = nil;
            if ([self scanObjcTypeEncodingIntoCDType:&subType]){
                //scannedType.typeName = [subType.typeName stringByAppendingString:@" *"];
                scannedType.objcEncoding = [NSString stringWithFormat:@"^%@",subType.objcEncoding];
                scannedType.structMembers=subType.structMembers;
                scannedType.pointer = YES;
            }
            break;
        }
        case _C_STRUCT_B:{
            // 2 forms {?==XXxx} and {Name=}
            NSString * structName = nil;
            NSInteger position =self.scanLocation;
    
            [self scanUpToCharactersFromString:@"=}" intoString:&structName];
            if (self.currentCharacter=='='){
                scannedType.objcEncoding = [scannedType.objcEncoding stringByAppendingFormat:@"%@=",structName];
                
                [self advance:1];
               NSMutableArray * structMembers = [NSMutableArray new];
                NSInteger idx = 0;
                while (self.currentCharacter != '}'){
                    CDType * structMember = nil;
                    if ([self scanObjcTypeEncodingIntoCDType:&structMember]){
                        if (structMember){
                            idx++;
                            structMember.varName = [NSString stringWithFormat:@"_field%ld",idx];
                            [structMembers addObject:structMember];
                            scannedType.objcEncoding = [scannedType.objcEncoding stringByAppendingString:structMember.objcEncoding];
                        }
                    }
                }
                if (structMembers.count){
                    scannedType.structMembers = structMembers;
                }
                scannedType.objcEncoding = [scannedType.objcEncoding stringByAppendingString:@"}"];
                if ([structName isEqualToString:@"?"]){
                    NSData * data = [[scannedType.objcEncoding decomposedStringWithCanonicalMapping] dataUsingEncoding:NSUTF8StringEncoding] ;
                    NSString * digest = [data sha1Digest];
                    
                    NSUInteger length = [digest length];
                    if (length > 8){
                        digest = [digest substringFromIndex:length - 8];
                    }
                    scannedType.typeName = [NSString stringWithFormat:@"struct CDStruct_%@", digest];
                }
                else{
                    scannedType.typeName = [NSString stringWithFormat:@"struct %@",structName];
                }
                
                NSMutableDictionary * cdStructs = NSThread.currentThread.threadDictionary[@"CDStructs"];
                if (!cdStructs){
                    cdStructs = [NSMutableDictionary new];
                    NSThread.currentThread.threadDictionary[@"CDStructs"] = cdStructs;
                }
                cdStructs[scannedType.typeName] = scannedType;
                
                [self advance:1];
            }
            else{
                [self setScanLocation:position];
                if ([self scanUpToString:@"}" intoString:&structName]){
                    scannedType.typeName=structName;
                    scannedType.objcEncoding=[NSString stringWithFormat:@"{%@}",structName];
                    [self advance:1];
                }
            }
            break;
        }
   
        case _C_CONST:{
            CDType * subType = nil;
            if ([self scanObjcTypeEncodingIntoCDType:&subType]){
                scannedType.typeName = [NSString stringWithFormat:@"const %@",subType.typeName];
                scannedType.objcEncoding = [NSString stringWithFormat:@"r%@",subType.objcEncoding];
            }
            break;
        }
       
        case _C_ARY_B:{
            NSInteger size = 0;
            [self scanInteger:&size];
            CDType * subType = nil;
            if ([self scanObjcTypeEncodingIntoCDType:&subType]){
                scannedType.typeName = [NSString stringWithFormat:@"%@[%ld]",subType.typeName,size];
                scannedType.objcEncoding = [NSString stringWithFormat:@"[%ld%@]",size,subType.objcEncoding];
                scannedType.structMembers = subType.structMembers;
            }
            [self advance:1];
        }
        
        case _C_ATOM:
        case _C_UNION_B:
        case _C_VECTOR:
            
        default:
            break;
    }
    BOOL success=self.scanLocation>startLocation;

    if (cdType){
        *cdType = success?scannedType:nil;
    }
    return success;
}

-(BOOL)scanMethodSignatureIntoCDTypes:(NSArray <CDType*> *_Nonnull *_Nullable)cdTypes{
    NSUInteger startLocation = self.scanLocation;
    NSMutableArray * types = [NSMutableArray new];
    while (!self.atEnd){
        CDType * type = nil;
        [self scanObjcTypeEncodingIntoCDType:&type];
        [self scanInteger:nil];
        if (type){
            [types addObject:type];
        }
        else{
            break;
        }
    }
    
    BOOL success=self.scanLocation>startLocation;
    if (cdTypes){
        *cdTypes = success?types:nil;
    }
    return success;
}
@end
