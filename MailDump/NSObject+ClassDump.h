//
//  NSObject+ClassDump.h
//  MailDump
//
//  Created by Scott Morrison on 2022-07-14.
//  Copyright © 2022 smallcubed. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (ClassDump)

@end

NS_ASSUME_NONNULL_END
