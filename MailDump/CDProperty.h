//
//  CDProperty.h
//  ClassDumper
//
//  Created by Scott Morrison on 2019-07-09.
//  Copyright © 2019 Indev Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
NS_ASSUME_NONNULL_BEGIN
@class CDType;
@class CDContext;

@interface CDProperty : NSObject <NSCopying>
@property (copy) NSString * name;
@property (strong) CDType * type;
@property (strong) NSArray <NSString *> * attributes;
@property (strong) NSString * comment;

@property (readonly) NSString* setterName;
@property (readonly) NSString* getterName;

+(instancetype)withObjcProperty:(objc_property_t)objcProperty context:(CDContext*)context;

+(CDProperty*)propertyWithLine:(NSString*)line;
-(NSArray <NSString*> * ) referencedClassNames;
-(NSArray <NSString*> * ) referencedProtocolNames;
-(NSArray  <CDType*> *) referencedStructs;
-(NSString*)declaration;
@end

NS_ASSUME_NONNULL_END
